class CreateCounselQuestions < ActiveRecord::Migration[5.0]
  def change
    create_table :counsel_questions do |t|
      t.string :question
      t.text :description
      t.references :asker, polymorphic: true

      t.timestamps
    end
  end
end
