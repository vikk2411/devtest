class CreateCustomerPayments < ActiveRecord::Migration[5.0]
  def change
    create_table :customer_payments do |t|
      t.string :id_token
      t.references :user, foreign_key: true
      t.references :professional, foreign_key: true
      t.decimal :amount
      t.decimal :paid_amount
      t.references :payable, polymorphic: true
      t.integer :status
      t.references :user_process, foreign_key: true
      t.references :scope, foreign_key: true

      t.timestamps
    end
    add_index :customer_payments, :id_token, unique: true
  end
end
