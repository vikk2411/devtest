class CreateQuotes < ActiveRecord::Migration[5.0]
  def change
    create_table :quotes do |t|
      t.references :user_process, foreign_key: true
      t.references :scope, foreign_key: true
      t.references :cart, foreign_key: true
      t.integer :status
      t.decimal :professional_fee
      t.decimal :government_fee
      t.decimal :oope
      t.boolean :service_tax
      t.decimal :tax
      t.datetime :validity_end_date

      t.timestamps
    end
  end
end
