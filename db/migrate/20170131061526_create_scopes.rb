class CreateScopes < ActiveRecord::Migration[5.0]
  def change
    create_table :scopes do |t|
      t.string :name
      t.references :user_process, foreign_key: true
      t.references :professional, foreign_key: true
      t.text :inclusions
      t.text :exclusions
      t.text :documents
      t.text :deliverables
      t.integer :status
      t.datetime :started_at
      t.datetime :closed_at
      t.integer :expected_duration

      t.timestamps
    end
  end
end
