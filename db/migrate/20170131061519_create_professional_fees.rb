class CreateProfessionalFees < ActiveRecord::Migration[5.0]
  def change
    create_table :professional_fees do |t|
      t.references :professional, foreign_key: true
      t.references :scenario, foreign_key: true
      t.references :location, foreign_key: true
      t.integer :status
      t.decimal :professional_fee
      t.decimal :government_fee
      t.decimal :oope
      t.boolean :service_tax
      t.decimal :tax

      t.timestamps
    end
  end
end
