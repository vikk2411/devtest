class CreateServiceFlowUserAnswerChoices < ActiveRecord::Migration[5.0]
  def change
    create_table :service_flow_user_answer_choices do |t|
      t.references :user, foreign_key: true
      t.references :answer_choice, foreign_key: {:to_table=>"service_flow_answer_choices"}, index: {:name=>"index_user_answer_choice_on_answer_choice_id"}
      t.references :user_process, foreign_key: true

      t.timestamps
    end
  end
end
