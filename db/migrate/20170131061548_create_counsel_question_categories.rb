class CreateCounselQuestionCategories < ActiveRecord::Migration[5.0]
  def change
    create_table :counsel_question_categories do |t|
      t.references :question, foreign_key: {:to_table=>"counsel_questions"}
      t.references :category, foreign_key: {:to_table=>"counsel_categories"}

      t.timestamps
    end
  end
end
