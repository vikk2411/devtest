class CreateProfessionalProfileEdits < ActiveRecord::Migration[5.0]
  def change
    create_table :professional_profile_edits do |t|
      t.references :professional, foreign_key: true
      t.datetime :experience_start_at
      t.string :company_name
      t.integer :location
      t.text :about
      t.text :company_profile
      t.text :education
      t.text :experience
      t.text :clients
      t.decimal :consult_rate

      t.timestamps
    end
  end
end
