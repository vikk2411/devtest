class CreateProfessionalSubServices < ActiveRecord::Migration[5.0]
  def change
    create_table :professional_sub_services do |t|
      t.references :professional, foreign_key: true
      t.references :sub_service, foreign_key: true
      t.references :professional_service, foreign_key: true

      t.timestamps
    end
  end
end
