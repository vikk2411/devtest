class CreateUserProcesses < ActiveRecord::Migration[5.0]
  def change
    create_table :user_processes do |t|
      t.string :id_token
      t.references :user, foreign_key: true
      t.references :professional, foreign_key: true
      t.references :service, foreign_key: true
      t.references :sub_service, foreign_key: true
      t.references :scenario, foreign_key: true
      t.integer :status
      t.text :external_link
      t.datetime :started_at
      t.datetime :closed_at
      t.integer :expected_duration

      t.timestamps
    end
    add_index :user_processes, :id_token, unique: true
  end
end
