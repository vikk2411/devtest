class CreateCounselAnswerRequests < ActiveRecord::Migration[5.0]
  def change
    create_table :counsel_answer_requests do |t|
      t.references :question, foreign_key: {:to_table=>"counsel_questions"}
      t.references :requester, polymorphic: true
      t.references :answerer, polymorphic: true

      t.timestamps
    end
  end
end
