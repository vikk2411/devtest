class CreateServiceFlowAnswerChoices < ActiveRecord::Migration[5.0]
  def change
    create_table :service_flow_answer_choices do |t|
      t.references :question, foreign_key: {:to_table=>"service_flow_questions"}
      t.text :answer
      t.integer :position
      t.text :next_questions

      t.timestamps
    end
  end
end
