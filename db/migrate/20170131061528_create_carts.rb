class CreateCarts < ActiveRecord::Migration[5.0]
  def change
    create_table :carts do |t|
      t.string :id_token
      t.references :user, foreign_key: true
      t.references :user_process, foreign_key: true

      t.timestamps
    end
    add_index :carts, :id_token, unique: true
  end
end
