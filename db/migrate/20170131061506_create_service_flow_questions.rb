class CreateServiceFlowQuestions < ActiveRecord::Migration[5.0]
  def change
    create_table :service_flow_questions do |t|
      t.text :question
      t.integer :question_type
      t.integer :parent

      t.timestamps
    end
    add_index :service_flow_questions, :parent
  end
end
