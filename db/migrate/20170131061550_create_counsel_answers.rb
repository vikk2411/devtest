class CreateCounselAnswers < ActiveRecord::Migration[5.0]
  def change
    create_table :counsel_answers do |t|
      t.references :question, foreign_key: {:to_table=>"counsel_questions"}
      t.text :answer
      t.references :answerer, polymorphic: true
      t.boolean :accepted
      t.boolean :deleted

      t.timestamps
    end
  end
end
