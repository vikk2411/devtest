class CreatePaymentRequests < ActiveRecord::Migration[5.0]
  def change
    create_table :payment_requests do |t|
      t.references :professional, foreign_key: true
      t.references :user_process, foreign_key: true
      t.references :scope, foreign_key: true
      t.decimal :amount
      t.integer :status
      t.text :rejection_reason
      t.integer :approver_id

      t.timestamps
    end
  end
end
