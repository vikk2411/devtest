class CreateProfessionals < ActiveRecord::Migration[5.0]
  def change
    create_table :professionals do |t|
      t.string :id_token
      t.references :user, foreign_key: true
      t.datetime :experience_start_at
      t.integer :source
      t.string :company_name
      t.integer :location
      t.string :photo
      t.text :about
      t.text :education
      t.text :experience
      t.text :clients
      t.decimal :aggregate_rating
      t.integer :status
      t.integer :professional_type
      t.text :other_details
      t.decimal :consult_rate
      t.text :platform_fee

      t.timestamps
    end
    add_index :professionals, :id_token, unique: true
  end
end
