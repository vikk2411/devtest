class CreateCounselVotes < ActiveRecord::Migration[5.0]
  def change
    create_table :counsel_votes do |t|
      t.references :voter, polymorphic: true
      t.references :votable, polymorphic: true
      t.integer :vote_type

      t.timestamps
    end
  end
end
