puts ">>> seeds.rb"

AdminUser.find_or_create_by!(email: "admin@wazzeer.com") do |new_user|
  new_user.name = "Super Admin"
  new_user.status = :active
  new_user.role = :super_admin
  new_user.password = "changeme"
  new_user.password_confirmation = "changeme"
  new_user.confirm
end

User.find_or_create_by!(email: "dinesh.gadge+wazzeeruser@gmail.com") do |new_user|
  new_user.name = "Generic User"
  new_user.status = :active
  new_user.role = :customer
  new_user.password = "changeme"
  new_user.password_confirmation = "changeme"
  new_user.confirm
end