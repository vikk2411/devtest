source 'https://rubygems.org'
git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end
ruby '2.4.0'
gem 'rails', '~> 5.0.1'
gem 'puma', '~> 3.0'
gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.2'
gem 'jquery-rails'
gem 'turbolinks', '~> 5'
gem 'jbuilder', '~> 2.5'
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
gem 'bootstrap-sass'
gem 'devise'
gem 'devise_invitable'
gem 'mysql2', '~> 0.3.18'
gem 'simple_form'
gem 'bootstrap3-datetimepicker-rails'
gem 'seedbank'
gem 'bullet'
gem 'cancancan'
gem 'delayed_job_active_record'
gem 'daemons'
gem 'whenever'
gem 'recaptcha', require: "recaptcha/rails"
gem 'faker'
gem 'cocoon'
gem 'paperclip', '~> 5.0.0.beta1'
gem 'fog'
gem 'aws-sdk-rails'
gem 'wicked_pdf'
gem 'wkhtmltopdf-binary'
gem 'dusen'
gem 'rubyzip'
gem 'paper_trail'
gem 'exception_notification'
gem 'httparty'
gem 'activerecord-session_store'

group :development do
  gem 'web-console', '>= 3.3.0'
  gem 'ruby-prof'
  gem 'listen', '~> 3.0.5'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'better_errors'
  gem 'binding_of_caller'
  gem 'foreman'
  gem 'guard-bundler'
  gem 'guard-rails'
  gem 'guard-rspec'
  gem 'rails_layout'
  gem 'rb-fchange', :require=>false
  gem 'rb-fsevent', :require=>false
  gem 'rb-inotify', :require=>false
  gem 'spring-commands-rspec'
  gem 'lol_dba'
  gem 'rails-erd'
end

group :development, :test do
  gem 'factory_girl_rails'
  gem 'rspec-rails'
  gem 'rubocop'
  gem 'byebug', platform: :mri
  gem 'mailcatcher'
end

group :test do
  gem 'capybara'
  gem 'database_cleaner'
  gem 'launchy'
  gem 'selenium-webdriver'
  gem 'shoulda-matchers', require: false
  gem 'simplecov', require: false
end
