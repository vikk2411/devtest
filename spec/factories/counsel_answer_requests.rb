FactoryGirl.define do
  factory :counsel_answer_request, class: 'Counsel::AnswerRequest' do
    question nil
    requester nil
    answerer nil
  end
end
