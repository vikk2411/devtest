FactoryGirl.define do
  factory :counsel_question_category, class: 'Counsel::QuestionCategory' do
    question nil
    category nil
  end
end
