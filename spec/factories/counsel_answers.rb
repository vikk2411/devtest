FactoryGirl.define do
  factory :counsel_answer, class: 'Counsel::Answer' do
    question nil
    answer "MyText"
    answerer nil
    accepted false
    deleted false
  end
end
