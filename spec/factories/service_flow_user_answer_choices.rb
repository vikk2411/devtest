FactoryGirl.define do
  factory :service_flow_user_answer_choice, class: 'ServiceFlow::UserAnswerChoice' do
    user nil
    answer_choice nil
    user_process nil
  end
end
