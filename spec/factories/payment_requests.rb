FactoryGirl.define do
  factory :payment_request do
    professional nil
    user_process nil
    scope nil
    amount "9.99"
    status 1
    rejection_reason "MyText"
    approver_id 1
  end
end
