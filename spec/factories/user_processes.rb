FactoryGirl.define do
  factory :user_process do
    id_token "MyString"
    user nil
    professional nil
    service nil
    sub_service nil
    scenario nil
    status 1
    external_link "MyText"
    started_at "2017-01-31 11:45:22"
    closed_at "2017-01-31 11:45:22"
    expected_duration 1
  end
end
