FactoryGirl.define do
  factory :service_flow_question, class: 'ServiceFlow::Question' do
    question "MyText"
    question_type 1
    parent 1
  end
end
