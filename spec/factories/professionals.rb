FactoryGirl.define do
  factory :professional do
    id_token "MyString"
    user nil
    experience_start_at "2017-01-31 11:45:11"
    source 1
    company_name "MyString"
    location 1
    photo "MyString"
    about "MyText"
    education "MyText"
    experience "MyText"
    clients "MyText"
    aggregate_rating "9.99"
    status 1
    professional_type 1
    other_details "MyText"
    consult_rate "9.99"
    platform_fee "MyText"
  end
end
