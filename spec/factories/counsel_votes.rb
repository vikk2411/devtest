FactoryGirl.define do
  factory :counsel_vote, class: 'Counsel::Vote' do
    voter nil
    votable nil
    vote_type 1
  end
end
