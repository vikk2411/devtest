FactoryGirl.define do
  factory :quote do
    user_process nil
    scope nil
    cart nil
    status 1
    professional_fee "9.99"
    government_fee "9.99"
    oope "9.99"
    service_tax false
    tax "9.99"
    validity_end_date "2017-01-31 11:45:30"
  end
end
