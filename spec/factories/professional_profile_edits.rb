FactoryGirl.define do
  factory :professional_profile_edit do
    professional nil
    experience_start_at "2017-01-31 11:45:13"
    company_name "MyString"
    location 1
    about "MyText"
    company_profile "MyText"
    education "MyText"
    experience "MyText"
    clients "MyText"
    consult_rate "9.99"
  end
end
