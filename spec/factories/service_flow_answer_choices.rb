FactoryGirl.define do
  factory :service_flow_answer_choice, class: 'ServiceFlow::AnswerChoice' do
    question nil
    answer "MyText"
    position 1
    next_questions "MyText"
  end
end
