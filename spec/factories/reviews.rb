FactoryGirl.define do
  factory :review do
    user nil
    professional nil
    rating "9.99"
    review "MyText"
    user_process nil
  end
end
