FactoryGirl.define do
  factory :counsel_question, class: 'Counsel::Question' do
    question "MyString"
    description "MyText"
    asker nil
  end
end
