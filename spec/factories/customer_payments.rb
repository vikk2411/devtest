FactoryGirl.define do
  factory :customer_payment do
    id_token "MyString"
    user nil
    professional nil
    amount "9.99"
    paid_amount "9.99"
    payable nil
    status 1
    user_process nil
    scope nil
  end
end
