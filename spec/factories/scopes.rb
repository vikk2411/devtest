FactoryGirl.define do
  factory :scope do
    name "MyString"
    user_process nil
    professional nil
    inclusions "MyText"
    exclusions "MyText"
    documents "MyText"
    deliverables "MyText"
    status 1
    started_at "2017-01-31 11:45:26"
    closed_at "2017-01-31 11:45:26"
    expected_duration 1
  end
end
