FactoryGirl.define do
  factory :professional_fee do
    professional nil
    scenario nil
    location nil
    status 1
    professional_fee "9.99"
    government_fee "9.99"
    oope "9.99"
    service_tax false
    tax "9.99"
  end
end
