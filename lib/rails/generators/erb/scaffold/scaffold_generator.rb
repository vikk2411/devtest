require 'rails/generators/erb'
require 'rails/generators/resource_helpers'

module Erb # :nodoc:
  module Generators # :nodoc:
    class ScaffoldGenerator < Base # :nodoc:
      include Rails::Generators::ResourceHelpers

      source_root File.expand_path("../templates", __FILE__)

      argument :attributes, type: :array, default: [], banner: "field:type field:type"

      def create_root_folder
        empty_directory File.join("app/views/admin", controller_file_path)
        empty_directory File.join("app/views/user", controller_file_path)
      end

      def copy_view_files
        available_views.each do |view|
          formats.each do |format|
            filename = filename_with_extensions(view, format)
            template "admin_#{filename}", File.join("app/views/admin", controller_file_path, filename)
            template "user_#{filename}", File.join("app/views/user", controller_file_path, filename)
          end
        end
      end

      def add_navigation_link
        admin_html = <<-HTML
<li class="">
  <a href="<%= admin_#{index_helper}_path %>"><%= fa('th-large') %> <span class="nav-label">#{plural_table_name.titleize}</span></a>
</li>
HTML
        inject_into_file 'app/views/layouts/_admin_navigation_links.html.erb', admin_html, before: "<%#END%>" 

        user_html = <<-HTML
<li class="">
  <a href="<%= user_#{index_helper}_path %>"><%= fa('th-large') %> <span class="nav-label">#{plural_table_name.titleize}</span></a>
</li>
HTML
        inject_into_file 'app/views/layouts/_user_navigation_links.html.erb', user_html, before: "<%#END%>"
      end

    protected

      def available_views
        %w(index edit show new _form)
      end

      def corrected_class_name
        class_name.include?("::") ? "::#{class_name}" : class_name
      end

      def object_name_or_human_name
        attributes_names.include?("name") ? "@#{singular_table_name}.name" : "'#{singular_table_name.titleize}'"
      end
    end
  end
end
