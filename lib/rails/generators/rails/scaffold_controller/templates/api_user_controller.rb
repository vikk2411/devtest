<% if namespaced? -%>
require_dependency "<%= namespaced_path %>/application_controller"

<% end -%>
<% module_namespacing do -%>
module User
  class <%= controller_class_name %>Controller < ApplicationController
    before_action :set_<%= singular_table_name %>, only: [:show, :update, :destroy]

    # GET /user<%= route_url %>
    # GET /user<%= route_url %>.json
    def index
      @<%= plural_table_name %> = <%= orm_class.all(corrected_class_name) %>
    end

    # GET /user<%= route_url %>/1
    # GET /user<%= route_url %>/1.json
    def show
    end

    # POST /user<%= route_url %>
    # POST /user<%= route_url %>.json
    def create
      @<%= singular_table_name %> = <%= orm_class.build(corrected_class_name, "#{singular_table_name}_params") %>

      if @<%= orm_instance.save %>
        render :show, status: :created, location: [:user, @<%= singular_table_name %>]
      else
        render json: @<%= orm_instance.errors %>, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /user<%= route_url %>/1
    # PATCH/PUT /user<%= route_url %>/1.json
    def update
      if @<%= orm_instance.update("#{singular_table_name}_params") %>
        render :show, status: :ok, location: [:user, <%= "@#{singular_table_name}" %>]
      else
        render json: @<%= orm_instance.errors %>, status: :unprocessable_entity
      end
    end

    # DELETE /user<%= route_url %>/1
    # DELETE /user<%= route_url %>/1.json
    def destroy
      @<%= orm_instance.destroy %>
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_<%= singular_table_name %>
        @<%= singular_table_name %> = <%= corrected_class_name %>.where(id: params[:id], user_id: current_user.id).first
      end

      # Only allow a trusted parameter "white list" through.
      def <%= "#{singular_table_name}_params" %>
        default_values = {user_id: current_user.id}
        <%- if attributes_names.empty? -%>
        params.fetch(:<%= singular_table_name %>, {})
        <%- else -%>
        params.require(:<%= singular_table_name %>).permit(<%= attributes_names.map { |name| ":#{name}" }.join(', ') %>).merge(default_values)
        <%- end -%>
      end
  end
end
<% end -%>
