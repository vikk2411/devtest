<% if namespaced? -%>
require_dependency "user/<%= namespaced_path %>/application_controller"

<% end -%>
<% module_namespacing do -%>
module User
  class <%= controller_class_name %>Controller < ApplicationController
    before_action :set_<%= singular_table_name %>, only: [:show, :edit, :update, :destroy]
    load_and_authorize_resource

    # GET /user<%= route_url %>
    # GET /user<%= route_url %>.json
    def index
      @<%= plural_table_name %> = <%= corrected_class_name %>.where(user_id: current_user.id)
    end

    # GET /user<%= route_url %>/1
    # GET /user<%= route_url %>/1.json
    def show
    end

    # GET /user<%= route_url %>/new
    def new
      @<%= singular_table_name %> = <%= orm_class.build(corrected_class_name) %>
    end

    # GET /user<%= route_url %>/1/edit
    def edit
    end

    # POST /user<%= route_url %>
    # POST /user<%= route_url %>.json
    def create
      <%- if attributes_names.include?("user_id") -%>
      @<%= singular_table_name %> = <%= orm_class.build(corrected_class_name, "#{singular_table_name}_params.merge(user: current_user)") %>
      <%- else -%>
      @<%= singular_table_name %> = <%= orm_class.build(corrected_class_name, "#{singular_table_name}_params") %>
      <%- end -%>
      respond_to do |format|
        if @<%= orm_instance.save %>
          format.html { redirect_to user_<%= index_helper %>_url, notice: <%= "'#{human_name} was successfully created.'" %> }
          format.json { render :show, status: :created, location: user_<%= index_helper %>_url }
        else
          format.html { render :new }
          format.json { render json: <%= "@#{orm_instance.errors}" %>, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /user<%= route_url %>/1
    # PATCH/PUT /user<%= route_url %>/1.json
    def update
      respond_to do |format|
        if @<%= orm_instance.update("#{singular_table_name}_params") %>
          format.html { redirect_to [:user, @<%= singular_table_name %>], notice: <%= "'#{human_name} was successfully updated.'" %> }
          format.json { render :show, status: :ok, location: [:user, <%= "@#{singular_table_name}" %>] }
        else
          format.html { render :edit }
          format.json { render json: <%= "@#{orm_instance.errors}" %>, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /user<%= route_url %>/1
    # DELETE /user<%= route_url %>/1.json
    def destroy
      @<%= orm_instance.destroy %>
      respond_to do |format|
        format.html { redirect_to user_<%= index_helper %>_url, notice: <%= "'#{human_name} was successfully destroyed.'" %> }
        format.json { head :no_content }
      end
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_<%= singular_table_name %>
        @<%= singular_table_name %> = <%= corrected_class_name %>.where(id: params[:id], user_id: current_user.id).first
        return invalid_access_flash_redirect_and_return if @<%= singular_table_name %>.blank?
      end

      # Only allow a trusted parameter "white list" through.
      def <%= "#{singular_table_name}_params" %>
        default_values = {user_id: current_user.id}
        <%- if attributes_names.empty? -%>
        params.fetch(:<%= singular_table_name %>, {})
        <%- else -%>
        params.require(:<%= singular_table_name %>).permit(<%= attributes_names.reject{|attr_name| ["user_id"].include?(attr_name) }.map { |name| ":#{name}" }.join(', ') %>).merge(default_values)
        <%- end -%>
      end
  end
end
<% end -%>
