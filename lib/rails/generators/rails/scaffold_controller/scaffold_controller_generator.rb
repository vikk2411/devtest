require 'rails/generators/resource_helpers'

module Rails
  module Generators
    class ScaffoldControllerGenerator < NamedBase # :nodoc:
      include ResourceHelpers

      source_root File.expand_path("../templates", __FILE__)

      check_class_collision prefix: "Admin::", suffix: "Controller"
      check_class_collision prefix: "User::", suffix: "Controller"

      class_option :helper, type: :boolean
      class_option :orm, banner: "NAME", type: :string, required: true,
                         desc: "ORM to generate the controller for dgit"
      class_option :api, type: :boolean,
                         desc: "Generates API controller"

      argument :attributes, type: :array, default: [], banner: "field:type field:type"

      def create_controller_files
        admin_template_file = options.api? ? "api_admin_controller.rb" : "admin_controller.rb"
        template admin_template_file, File.join('app/controllers/admin', controller_class_path, "#{controller_file_name}_controller.rb")

        user_template_file = options.api? ? "api_user_controller.rb" : "user_controller.rb"
        template user_template_file, File.join('app/controllers/user', controller_class_path, "#{controller_file_name}_controller.rb")
      end

      hook_for :template_engine, :test_framework, as: :scaffold


      # Invoke the helper using the controller name (pluralized)
      hook_for :helper, as: :scaffold do |invoked|
        invoke invoked, [ controller_name ]
      end
      
      hook_for :jbuilder

      protected
        def corrected_class_name
          class_name.include?("::") ? "::#{class_name}" : class_name
        end
    end
  end
end
