namespace :db do
  desc "Prerequisite for other tasks"
  task :connect do
    ActiveRecord::Base.establish_connection(YAML.load_file("#{Rails.root}/config/database.yml")[Rails.env])
  end
  desc "Drop and create database, run migrations and seed database"
  task :install do
    # Do not include erd generation logic here as it disables redundant associations
    # and redundant columns are not populated in seeding

    # Kill rails processes, otherwise we get an error due to postgres connections to database
    # system("ps ax | grep rails | grep -v grep | awk '{print $1}' | xargs kill -9") if Rails.env.development?
    start_time = Time.now
    Rake::Task['db:migrate:reset'].invoke
    ENV['seed'] = "true"
    ApplicationRecord.transaction do
      Rake::Task['db:seed:development'].invoke
    end
    end_time = Time.now
    diff_time = end_time - start_time
    mm, ss = diff_time.divmod(60) 
    hh, mm = mm.divmod(60) 
    puts "Elapsed time: [#{hh} hours, #{mm} minutes, #{ss.to_i} seconds]" if (hh + mm + ss.to_i)!=0
  end
  namespace :erd do
    desc "Generate ERD diagram"
    task :reset do
      ENV['diagram'] = "true"
      ENV['attributes'] = "foreign_keys,content"
      ENV['indirect'] = "false"
      Rake::Task['erd'].invoke
    end
  end
=begin  
  desc "Create Table Space directory Eg: rake db:create_tablespace[2015,2016,2017]"
  task :create_tablespace do |t, args|
    if args.extras.count == 0
      puts "Provide year for which the table space is to be created"
    else
      if Dir.exists?("/cot/#{SCHOOL_CONFIG['school_name']}")
        puts "Root table space located"
      else
        if not Dir.exists?("/cot")
          puts "Creating /cot"
          system("sudo mkdir /cot")
          system("sudo chown postgres.postgres /cot")
        end
        puts "Creating /cot/#{SCHOOL_CONFIG['school_name']}"
        system("sudo mkdir /cot/#{SCHOOL_CONFIG['school_name']}")
        system("sudo chown postgres.postgres /cot/#{SCHOOL_CONFIG['school_name']}")
      end
      years = args.extras
      years.each do |year|
        if not Dir.exists?("/cot/#{SCHOOL_CONFIG['school_name']}/batch#{year}")
          puts "Creating tablespace: batch#{year}"
          system("sudo mkdir /cot/#{SCHOOL_CONFIG['school_name']}/batch#{year}")
          puts "Directory /cot/#{SCHOOL_CONFIG['school_name']}/batch#{year} created"
          system("sudo chown postgres.postgres /cot/#{SCHOOL_CONFIG['school_name']}/batch#{year}")
          puts "Permission Changed"
          system("psql -U postgres -c \"CREATE TABLESPACE batch#{year} OWNER campoff LOCATION '/cot/#{SCHOOL_CONFIG['school_name']}/batch#{year}'\"")
          puts "Table Space batch#{year} created in Database with mapping to /cot/#{SCHOOL_CONFIG['school_name']}/batch#{year}"
        else
          puts "Directory /cot/#{SCHOOL_CONFIG['school_name']}/batch#{year} already exists"
        end
      end
    end
  end
  desc "Drop Table Space directory Eg: rake db:drop_tablespace[2015,2016,2017]"
  task :drop_tablespace do |t, args|
    if args.extras.count == 0
      puts "Provide year for which the table space is to be dropped"
    else
      years = args.extras
      years.each do |year|
        if Dir.exists?("/cot/#{SCHOOL_CONFIG['school_name']}/batch#{year}")
          system("psql -U postgres -c \"DROP TABLESPACE batch#{year}\"")
          system("sudo rm -rf /cot/#{SCHOOL_CONFIG['school_name']}/batch#{year}")
        else
          puts "Directory /cot/#{SCHOOL_CONFIG['school_name']}/batch#{year} does not exist"
        end
      end
    end
  end
  desc "Dump all databases"
  task :dump do
    dump_file = "dump_#{Time.now.strftime("%Y-%m-%d_%H-%M-%S")}.sql.gz"
    system("pg_dumpall -U postgres | gzip > #{Rails.root}/sqldumps/#{dump_file}")
  end
  desc "Restore all databases"
  task :restore do |t, args| 
    if args.extras.count == 0
      puts "Provide sql dump which is to be restored"
    else
      dump_file = args.extras[0]
      puts "Restoring database using: #{dump_file}"
      system("gunzip -c #{dump_file} | psql -U postgres > #{Rails.root}/sqldumps/restore.log")
    end 
  end
=end

end
