var app = app || {}
app.common = app.common || {}

app.common.functions = {
  highlightNavigationLink: function(){
    options = {}
    if($('#forced-nav-link-highlight').length){
      options.url = $('#forced-nav-link-highlight').attr('data-url')
      options.selector = $('#forced-nav-link-highlight').attr('data-selector')
    }

    function matching_navigation_link(url){
      var links = $("ul#side-menu.nav a[href!='']").filter(function() {
        return this.href == url || url.indexOf(this.href) == 0;
      });
      return $(links.sort(function (a, b) { return b.href.length - a.href.length; })[0])
    }

    function highlight_navigation_link(link_element){
      var element = link_element.parent().addClass('active').parent().addClass('in').parent();
      if (element.is('li')) {
        element.addClass('active');
      }
    }

    if(options.selector){
      highlight_navigation_link($(options.selector));
      return true;
    }

    var active_child_hyperlink = matching_navigation_link(options.url || window.location.href.replace(/#$/, ''));
    // console.log(active_child_hyperlink);
    // if (active_child_hyperlinks.length > 1){
      // alert("Multiple matching links: " + active_child_hyperlinks.length.toString());
      // active_child_hyperlink = active_child_hyperlinks.sort(function (a, b) { return b.length - a.length; })[0];
    // }
    if (!!active_child_hyperlink){
      highlight_navigation_link(active_child_hyperlink);
    }
    else if (!active_child_hyperlink){
      active_child_hyperlink = matching_navigation_link('');
      highlight_navigation_link(active_child_hyperlink);
    } 
  }
}

app.common.events = {
  bindEvents: function() {
    $(".dropdown").on('show.bs.dropdown', function () {
      $(".select-overlay").show();
    });
    $(".dropdown").on('hidden.bs.dropdown', function () {
      $(".select-overlay").hide();
    });
    $(document).on('ready', function(){
      app.common.functions.highlightNavigationLink();
      $('body').tooltip({
        selector: "[data-toggle=tooltip]",
        container: "body"
      });
      $('#wrapper').slimScroll({
        height: '100%',
        railOpacity: 0.4,
        wheelStep: 10
      });
      $('.sidebar-collapse').slimScroll({
        height: '100%',
        railOpacity: 0.4,
        wheelStep: 10
      });
      $(".slimScrollBar").mouseover();
      $('.i-checks').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green',
      });
    });
  },
}