class VisitorsController < ApplicationController
  layout 'home.inspinia', only: :index

  def index
    if user_signed_in?
      redirect_to user_dashboard_path
    elsif admin_user_signed_in?
      redirect_to admin_dashboard_path
    else
      
    end
  end
end
