module Admin
  class UserProcessesController < ApplicationController
    before_action :set_user_process, only: [:show, :edit, :update, :destroy]
    load_and_authorize_resource

    # GET /admin/user_processes
    # GET /admin/user_processes.json
    def index
      @user_processes = UserProcess.all
    end

    # GET /admin/user_processes/1
    # GET /admin/user_processes/1.json
    def show
    end

    # GET /admin/user_processes/new
    def new
      @user_process = UserProcess.new
    end

    # GET /admin/user_processes/1/edit
    def edit
    end

    # POST /admin/user_processes
    # POST /admin/user_processes.json
    def create
      @user_process = UserProcess.new(user_process_params)

      respond_to do |format|
        if @user_process.save
          format.html { redirect_to admin_user_processes_url, notice: 'User process was successfully created.' }
          format.json { render :show, status: :created, location: admin_user_processes_url }
        else
          format.html { render :new }
          format.json { render json: @user_process.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /admin/user_processes/1
    # PATCH/PUT /admin/user_processes/1.json
    def update
      respond_to do |format|
        if @user_process.update(user_process_params)
          format.html { redirect_to [:admin, @user_process], notice: 'User process was successfully updated.' }
          format.json { render :show, status: :ok, location: [:admin, @user_process] }
        else
          format.html { render :edit }
          format.json { render json: @user_process.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /admin/user_processes/1
    # DELETE /admin/user_processes/1.json
    def destroy
      @user_process.destroy
      respond_to do |format|
        format.html { redirect_to admin_user_processes_url, notice: 'User process was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_user_process
        @user_process = UserProcess.where(id: params[:id]).first
        return invalid_access_flash_redirect_and_return if @user_process.blank?
      end

      # Only allow a trusted parameter "white list" through.
      def user_process_params
        default_values = {}
        params.require(:user_process).permit(:id_token, :user_id, :professional_id, :service_id, :sub_service_id, :scenario_id, :status, :external_link, :started_at, :closed_at, :expected_duration).merge(default_values)
      end
  end
end
