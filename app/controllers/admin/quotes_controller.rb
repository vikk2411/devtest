module Admin
  class QuotesController < ApplicationController
    before_action :set_quote, only: [:show, :edit, :update, :destroy]
    load_and_authorize_resource

    # GET /admin/quotes
    # GET /admin/quotes.json
    def index
      @quotes = Quote.all
    end

    # GET /admin/quotes/1
    # GET /admin/quotes/1.json
    def show
    end

    # GET /admin/quotes/new
    def new
      @quote = Quote.new
    end

    # GET /admin/quotes/1/edit
    def edit
    end

    # POST /admin/quotes
    # POST /admin/quotes.json
    def create
      @quote = Quote.new(quote_params)

      respond_to do |format|
        if @quote.save
          format.html { redirect_to admin_quotes_url, notice: 'Quote was successfully created.' }
          format.json { render :show, status: :created, location: admin_quotes_url }
        else
          format.html { render :new }
          format.json { render json: @quote.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /admin/quotes/1
    # PATCH/PUT /admin/quotes/1.json
    def update
      respond_to do |format|
        if @quote.update(quote_params)
          format.html { redirect_to [:admin, @quote], notice: 'Quote was successfully updated.' }
          format.json { render :show, status: :ok, location: [:admin, @quote] }
        else
          format.html { render :edit }
          format.json { render json: @quote.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /admin/quotes/1
    # DELETE /admin/quotes/1.json
    def destroy
      @quote.destroy
      respond_to do |format|
        format.html { redirect_to admin_quotes_url, notice: 'Quote was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_quote
        @quote = Quote.where(id: params[:id]).first
        return invalid_access_flash_redirect_and_return if @quote.blank?
      end

      # Only allow a trusted parameter "white list" through.
      def quote_params
        default_values = {}
        params.require(:quote).permit(:user_process_id, :scope_id, :cart_id, :status, :professional_fee, :government_fee, :oope, :service_tax, :tax, :validity_end_date).merge(default_values)
      end
  end
end
