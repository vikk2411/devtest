module Admin
  class PaymentRequestsController < ApplicationController
    before_action :set_payment_request, only: [:show, :edit, :update, :destroy]
    load_and_authorize_resource

    # GET /admin/payment_requests
    # GET /admin/payment_requests.json
    def index
      @payment_requests = PaymentRequest.all
    end

    # GET /admin/payment_requests/1
    # GET /admin/payment_requests/1.json
    def show
    end

    # GET /admin/payment_requests/new
    def new
      @payment_request = PaymentRequest.new
    end

    # GET /admin/payment_requests/1/edit
    def edit
    end

    # POST /admin/payment_requests
    # POST /admin/payment_requests.json
    def create
      @payment_request = PaymentRequest.new(payment_request_params)

      respond_to do |format|
        if @payment_request.save
          format.html { redirect_to admin_payment_requests_url, notice: 'Payment request was successfully created.' }
          format.json { render :show, status: :created, location: admin_payment_requests_url }
        else
          format.html { render :new }
          format.json { render json: @payment_request.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /admin/payment_requests/1
    # PATCH/PUT /admin/payment_requests/1.json
    def update
      respond_to do |format|
        if @payment_request.update(payment_request_params)
          format.html { redirect_to [:admin, @payment_request], notice: 'Payment request was successfully updated.' }
          format.json { render :show, status: :ok, location: [:admin, @payment_request] }
        else
          format.html { render :edit }
          format.json { render json: @payment_request.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /admin/payment_requests/1
    # DELETE /admin/payment_requests/1.json
    def destroy
      @payment_request.destroy
      respond_to do |format|
        format.html { redirect_to admin_payment_requests_url, notice: 'Payment request was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_payment_request
        @payment_request = PaymentRequest.where(id: params[:id]).first
        return invalid_access_flash_redirect_and_return if @payment_request.blank?
      end

      # Only allow a trusted parameter "white list" through.
      def payment_request_params
        default_values = {}
        params.require(:payment_request).permit(:professional_id, :user_process_id, :scope_id, :amount, :status, :rejection_reason, :approver_id).merge(default_values)
      end
  end
end
