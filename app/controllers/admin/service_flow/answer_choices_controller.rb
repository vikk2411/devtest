module Admin
  class ServiceFlow::AnswerChoicesController < ApplicationController
    before_action :set_service_flow_answer_choice, only: [:show, :edit, :update, :destroy]
    load_and_authorize_resource

    # GET /admin/service_flow/answer_choices
    # GET /admin/service_flow/answer_choices.json
    def index
      @service_flow_answer_choices = ::ServiceFlow::AnswerChoice.all
    end

    # GET /admin/service_flow/answer_choices/1
    # GET /admin/service_flow/answer_choices/1.json
    def show
    end

    # GET /admin/service_flow/answer_choices/new
    def new
      @service_flow_answer_choice = ::ServiceFlow::AnswerChoice.new
    end

    # GET /admin/service_flow/answer_choices/1/edit
    def edit
    end

    # POST /admin/service_flow/answer_choices
    # POST /admin/service_flow/answer_choices.json
    def create
      @service_flow_answer_choice = ::ServiceFlow::AnswerChoice.new(service_flow_answer_choice_params)

      respond_to do |format|
        if @service_flow_answer_choice.save
          format.html { redirect_to admin_service_flow_answer_choices_url, notice: 'Answer choice was successfully created.' }
          format.json { render :show, status: :created, location: admin_service_flow_answer_choices_url }
        else
          format.html { render :new }
          format.json { render json: @service_flow_answer_choice.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /admin/service_flow/answer_choices/1
    # PATCH/PUT /admin/service_flow/answer_choices/1.json
    def update
      respond_to do |format|
        if @service_flow_answer_choice.update(service_flow_answer_choice_params)
          format.html { redirect_to [:admin, @service_flow_answer_choice], notice: 'Answer choice was successfully updated.' }
          format.json { render :show, status: :ok, location: [:admin, @service_flow_answer_choice] }
        else
          format.html { render :edit }
          format.json { render json: @service_flow_answer_choice.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /admin/service_flow/answer_choices/1
    # DELETE /admin/service_flow/answer_choices/1.json
    def destroy
      @service_flow_answer_choice.destroy
      respond_to do |format|
        format.html { redirect_to admin_service_flow_answer_choices_url, notice: 'Answer choice was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_service_flow_answer_choice
        @service_flow_answer_choice = ::ServiceFlow::AnswerChoice.where(id: params[:id]).first
        return invalid_access_flash_redirect_and_return if @service_flow_answer_choice.blank?
      end

      # Only allow a trusted parameter "white list" through.
      def service_flow_answer_choice_params
        default_values = {}
        params.require(:service_flow_answer_choice).permit(:question_id, :answer, :position, :next_questions).merge(default_values)
      end
  end
end
