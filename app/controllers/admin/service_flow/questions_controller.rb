module Admin
  class ServiceFlow::QuestionsController < ApplicationController
    before_action :set_service_flow_question, only: [:show, :edit, :update, :destroy]
    load_and_authorize_resource

    # GET /admin/service_flow/questions
    # GET /admin/service_flow/questions.json
    def index
      @service_flow_questions = ::ServiceFlow::Question.all
    end

    # GET /admin/service_flow/questions/1
    # GET /admin/service_flow/questions/1.json
    def show
    end

    # GET /admin/service_flow/questions/new
    def new
      @service_flow_question = ::ServiceFlow::Question.new
    end

    # GET /admin/service_flow/questions/1/edit
    def edit
    end

    # POST /admin/service_flow/questions
    # POST /admin/service_flow/questions.json
    def create
      @service_flow_question = ::ServiceFlow::Question.new(service_flow_question_params)

      respond_to do |format|
        if @service_flow_question.save
          format.html { redirect_to admin_service_flow_questions_url, notice: 'Question was successfully created.' }
          format.json { render :show, status: :created, location: admin_service_flow_questions_url }
        else
          format.html { render :new }
          format.json { render json: @service_flow_question.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /admin/service_flow/questions/1
    # PATCH/PUT /admin/service_flow/questions/1.json
    def update
      respond_to do |format|
        if @service_flow_question.update(service_flow_question_params)
          format.html { redirect_to [:admin, @service_flow_question], notice: 'Question was successfully updated.' }
          format.json { render :show, status: :ok, location: [:admin, @service_flow_question] }
        else
          format.html { render :edit }
          format.json { render json: @service_flow_question.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /admin/service_flow/questions/1
    # DELETE /admin/service_flow/questions/1.json
    def destroy
      @service_flow_question.destroy
      respond_to do |format|
        format.html { redirect_to admin_service_flow_questions_url, notice: 'Question was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_service_flow_question
        @service_flow_question = ::ServiceFlow::Question.where(id: params[:id]).first
        return invalid_access_flash_redirect_and_return if @service_flow_question.blank?
      end

      # Only allow a trusted parameter "white list" through.
      def service_flow_question_params
        default_values = {}
        params.require(:service_flow_question).permit(:question, :question_type, :parent).merge(default_values)
      end
  end
end
