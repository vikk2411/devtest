module Admin
  class ProfessionalSubServicesController < ApplicationController
    before_action :set_professional_sub_service, only: [:show, :edit, :update, :destroy]
    load_and_authorize_resource

    # GET /admin/professional_sub_services
    # GET /admin/professional_sub_services.json
    def index
      @professional_sub_services = ProfessionalSubService.all
    end

    # GET /admin/professional_sub_services/1
    # GET /admin/professional_sub_services/1.json
    def show
    end

    # GET /admin/professional_sub_services/new
    def new
      @professional_sub_service = ProfessionalSubService.new
    end

    # GET /admin/professional_sub_services/1/edit
    def edit
    end

    # POST /admin/professional_sub_services
    # POST /admin/professional_sub_services.json
    def create
      @professional_sub_service = ProfessionalSubService.new(professional_sub_service_params)

      respond_to do |format|
        if @professional_sub_service.save
          format.html { redirect_to admin_professional_sub_services_url, notice: 'Professional sub service was successfully created.' }
          format.json { render :show, status: :created, location: admin_professional_sub_services_url }
        else
          format.html { render :new }
          format.json { render json: @professional_sub_service.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /admin/professional_sub_services/1
    # PATCH/PUT /admin/professional_sub_services/1.json
    def update
      respond_to do |format|
        if @professional_sub_service.update(professional_sub_service_params)
          format.html { redirect_to [:admin, @professional_sub_service], notice: 'Professional sub service was successfully updated.' }
          format.json { render :show, status: :ok, location: [:admin, @professional_sub_service] }
        else
          format.html { render :edit }
          format.json { render json: @professional_sub_service.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /admin/professional_sub_services/1
    # DELETE /admin/professional_sub_services/1.json
    def destroy
      @professional_sub_service.destroy
      respond_to do |format|
        format.html { redirect_to admin_professional_sub_services_url, notice: 'Professional sub service was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_professional_sub_service
        @professional_sub_service = ProfessionalSubService.where(id: params[:id]).first
        return invalid_access_flash_redirect_and_return if @professional_sub_service.blank?
      end

      # Only allow a trusted parameter "white list" through.
      def professional_sub_service_params
        default_values = {}
        params.require(:professional_sub_service).permit(:professional_id, :sub_service_id, :professional_service_id).merge(default_values)
      end
  end
end
