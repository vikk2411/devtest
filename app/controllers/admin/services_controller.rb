module Admin
  class ServicesController < ApplicationController
    before_action :set_service, only: [:show, :edit, :update, :destroy]
    load_and_authorize_resource

    # GET /admin/services
    # GET /admin/services.json
    def index
      @services = Service.all
    end

    # GET /admin/services/1
    # GET /admin/services/1.json
    def show
    end

    # GET /admin/services/new
    def new
      @service = Service.new
    end

    # GET /admin/services/1/edit
    def edit
    end

    # POST /admin/services
    # POST /admin/services.json
    def create
      @service = Service.new(service_params)

      respond_to do |format|
        if @service.save
          format.html { redirect_to admin_services_url, notice: 'Service was successfully created.' }
          format.json { render :show, status: :created, location: admin_services_url }
        else
          format.html { render :new }
          format.json { render json: @service.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /admin/services/1
    # PATCH/PUT /admin/services/1.json
    def update
      respond_to do |format|
        if @service.update(service_params)
          format.html { redirect_to [:admin, @service], notice: 'Service was successfully updated.' }
          format.json { render :show, status: :ok, location: [:admin, @service] }
        else
          format.html { render :edit }
          format.json { render json: @service.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /admin/services/1
    # DELETE /admin/services/1.json
    def destroy
      @service.destroy
      respond_to do |format|
        format.html { redirect_to admin_services_url, notice: 'Service was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_service
        @service = Service.where(id: params[:id]).first
        return invalid_access_flash_redirect_and_return if @service.blank?
      end

      # Only allow a trusted parameter "white list" through.
      def service_params
        default_values = {}
        params.require(:service).permit(:name).merge(default_values)
      end
  end
end
