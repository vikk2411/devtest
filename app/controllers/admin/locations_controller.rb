module Admin
  class LocationsController < ApplicationController
    before_action :set_location, only: [:show, :edit, :update, :destroy]
    load_and_authorize_resource

    # GET /admin/locations
    # GET /admin/locations.json
    def index
      @locations = Location.all
    end

    # GET /admin/locations/1
    # GET /admin/locations/1.json
    def show
    end

    # GET /admin/locations/new
    def new
      @location = Location.new
    end

    # GET /admin/locations/1/edit
    def edit
    end

    # POST /admin/locations
    # POST /admin/locations.json
    def create
      @location = Location.new(location_params)

      respond_to do |format|
        if @location.save
          format.html { redirect_to admin_locations_url, notice: 'Location was successfully created.' }
          format.json { render :show, status: :created, location: admin_locations_url }
        else
          format.html { render :new }
          format.json { render json: @location.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /admin/locations/1
    # PATCH/PUT /admin/locations/1.json
    def update
      respond_to do |format|
        if @location.update(location_params)
          format.html { redirect_to [:admin, @location], notice: 'Location was successfully updated.' }
          format.json { render :show, status: :ok, location: [:admin, @location] }
        else
          format.html { render :edit }
          format.json { render json: @location.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /admin/locations/1
    # DELETE /admin/locations/1.json
    def destroy
      @location.destroy
      respond_to do |format|
        format.html { redirect_to admin_locations_url, notice: 'Location was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_location
        @location = Location.where(id: params[:id]).first
        return invalid_access_flash_redirect_and_return if @location.blank?
      end

      # Only allow a trusted parameter "white list" through.
      def location_params
        default_values = {}
        params.require(:location).permit(:name).merge(default_values)
      end
  end
end
