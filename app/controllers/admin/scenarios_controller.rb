module Admin
  class ScenariosController < ApplicationController
    before_action :set_scenario, only: [:show, :edit, :update, :destroy]
    load_and_authorize_resource

    # GET /admin/scenarios
    # GET /admin/scenarios.json
    def index
      @scenarios = Scenario.all
    end

    # GET /admin/scenarios/1
    # GET /admin/scenarios/1.json
    def show
    end

    # GET /admin/scenarios/new
    def new
      @scenario = Scenario.new
    end

    # GET /admin/scenarios/1/edit
    def edit
    end

    # POST /admin/scenarios
    # POST /admin/scenarios.json
    def create
      @scenario = Scenario.new(scenario_params)

      respond_to do |format|
        if @scenario.save
          format.html { redirect_to admin_scenarios_url, notice: 'Scenario was successfully created.' }
          format.json { render :show, status: :created, location: admin_scenarios_url }
        else
          format.html { render :new }
          format.json { render json: @scenario.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /admin/scenarios/1
    # PATCH/PUT /admin/scenarios/1.json
    def update
      respond_to do |format|
        if @scenario.update(scenario_params)
          format.html { redirect_to [:admin, @scenario], notice: 'Scenario was successfully updated.' }
          format.json { render :show, status: :ok, location: [:admin, @scenario] }
        else
          format.html { render :edit }
          format.json { render json: @scenario.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /admin/scenarios/1
    # DELETE /admin/scenarios/1.json
    def destroy
      @scenario.destroy
      respond_to do |format|
        format.html { redirect_to admin_scenarios_url, notice: 'Scenario was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_scenario
        @scenario = Scenario.where(id: params[:id]).first
        return invalid_access_flash_redirect_and_return if @scenario.blank?
      end

      # Only allow a trusted parameter "white list" through.
      def scenario_params
        default_values = {}
        params.fetch(:scenario, {})
      end
  end
end
