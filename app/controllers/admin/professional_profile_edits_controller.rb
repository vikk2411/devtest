module Admin
  class ProfessionalProfileEditsController < ApplicationController
    before_action :set_professional_profile_edit, only: [:show, :edit, :update, :destroy]
    load_and_authorize_resource

    # GET /admin/professional_profile_edits
    # GET /admin/professional_profile_edits.json
    def index
      @professional_profile_edits = ProfessionalProfileEdit.all
    end

    # GET /admin/professional_profile_edits/1
    # GET /admin/professional_profile_edits/1.json
    def show
    end

    # GET /admin/professional_profile_edits/new
    def new
      @professional_profile_edit = ProfessionalProfileEdit.new
    end

    # GET /admin/professional_profile_edits/1/edit
    def edit
    end

    # POST /admin/professional_profile_edits
    # POST /admin/professional_profile_edits.json
    def create
      @professional_profile_edit = ProfessionalProfileEdit.new(professional_profile_edit_params)

      respond_to do |format|
        if @professional_profile_edit.save
          format.html { redirect_to admin_professional_profile_edits_url, notice: 'Professional profile edit was successfully created.' }
          format.json { render :show, status: :created, location: admin_professional_profile_edits_url }
        else
          format.html { render :new }
          format.json { render json: @professional_profile_edit.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /admin/professional_profile_edits/1
    # PATCH/PUT /admin/professional_profile_edits/1.json
    def update
      respond_to do |format|
        if @professional_profile_edit.update(professional_profile_edit_params)
          format.html { redirect_to [:admin, @professional_profile_edit], notice: 'Professional profile edit was successfully updated.' }
          format.json { render :show, status: :ok, location: [:admin, @professional_profile_edit] }
        else
          format.html { render :edit }
          format.json { render json: @professional_profile_edit.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /admin/professional_profile_edits/1
    # DELETE /admin/professional_profile_edits/1.json
    def destroy
      @professional_profile_edit.destroy
      respond_to do |format|
        format.html { redirect_to admin_professional_profile_edits_url, notice: 'Professional profile edit was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_professional_profile_edit
        @professional_profile_edit = ProfessionalProfileEdit.where(id: params[:id]).first
        return invalid_access_flash_redirect_and_return if @professional_profile_edit.blank?
      end

      # Only allow a trusted parameter "white list" through.
      def professional_profile_edit_params
        default_values = {}
        params.require(:professional_profile_edit).permit(:professional_id, :experience_start_at, :company_name, :location, :about, :company_profile, :education, :experience, :clients, :consult_rate).merge(default_values)
      end
  end
end
