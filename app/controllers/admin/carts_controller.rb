module Admin
  class CartsController < ApplicationController
    before_action :set_cart, only: [:show, :edit, :update, :destroy]
    load_and_authorize_resource

    # GET /admin/carts
    # GET /admin/carts.json
    def index
      @carts = Cart.all
    end

    # GET /admin/carts/1
    # GET /admin/carts/1.json
    def show
    end

    # GET /admin/carts/new
    def new
      @cart = Cart.new
    end

    # GET /admin/carts/1/edit
    def edit
    end

    # POST /admin/carts
    # POST /admin/carts.json
    def create
      @cart = Cart.new(cart_params)

      respond_to do |format|
        if @cart.save
          format.html { redirect_to admin_carts_url, notice: 'Cart was successfully created.' }
          format.json { render :show, status: :created, location: admin_carts_url }
        else
          format.html { render :new }
          format.json { render json: @cart.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /admin/carts/1
    # PATCH/PUT /admin/carts/1.json
    def update
      respond_to do |format|
        if @cart.update(cart_params)
          format.html { redirect_to [:admin, @cart], notice: 'Cart was successfully updated.' }
          format.json { render :show, status: :ok, location: [:admin, @cart] }
        else
          format.html { render :edit }
          format.json { render json: @cart.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /admin/carts/1
    # DELETE /admin/carts/1.json
    def destroy
      @cart.destroy
      respond_to do |format|
        format.html { redirect_to admin_carts_url, notice: 'Cart was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_cart
        @cart = Cart.where(id: params[:id]).first
        return invalid_access_flash_redirect_and_return if @cart.blank?
      end

      # Only allow a trusted parameter "white list" through.
      def cart_params
        default_values = {}
        params.require(:cart).permit(:id_token, :user_id, :user_process_id).merge(default_values)
      end
  end
end
