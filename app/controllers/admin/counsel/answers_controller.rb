module Admin
  class Counsel::AnswersController < ApplicationController
    before_action :set_counsel_answer, only: [:show, :edit, :update, :destroy]
    load_and_authorize_resource

    # GET /admin/counsel/answers
    # GET /admin/counsel/answers.json
    def index
      @counsel_answers = ::Counsel::Answer.all
    end

    # GET /admin/counsel/answers/1
    # GET /admin/counsel/answers/1.json
    def show
    end

    # GET /admin/counsel/answers/new
    def new
      @counsel_answer = ::Counsel::Answer.new
    end

    # GET /admin/counsel/answers/1/edit
    def edit
    end

    # POST /admin/counsel/answers
    # POST /admin/counsel/answers.json
    def create
      @counsel_answer = ::Counsel::Answer.new(counsel_answer_params)

      respond_to do |format|
        if @counsel_answer.save
          format.html { redirect_to admin_counsel_answers_url, notice: 'Answer was successfully created.' }
          format.json { render :show, status: :created, location: admin_counsel_answers_url }
        else
          format.html { render :new }
          format.json { render json: @counsel_answer.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /admin/counsel/answers/1
    # PATCH/PUT /admin/counsel/answers/1.json
    def update
      respond_to do |format|
        if @counsel_answer.update(counsel_answer_params)
          format.html { redirect_to [:admin, @counsel_answer], notice: 'Answer was successfully updated.' }
          format.json { render :show, status: :ok, location: [:admin, @counsel_answer] }
        else
          format.html { render :edit }
          format.json { render json: @counsel_answer.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /admin/counsel/answers/1
    # DELETE /admin/counsel/answers/1.json
    def destroy
      @counsel_answer.destroy
      respond_to do |format|
        format.html { redirect_to admin_counsel_answers_url, notice: 'Answer was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_counsel_answer
        @counsel_answer = ::Counsel::Answer.where(id: params[:id]).first
        return invalid_access_flash_redirect_and_return if @counsel_answer.blank?
      end

      # Only allow a trusted parameter "white list" through.
      def counsel_answer_params
        default_values = {}
        params.require(:counsel_answer).permit(:question_id, :answer, :answerer_id, :answerer_type, :accepted, :deleted).merge(default_values)
      end
  end
end
