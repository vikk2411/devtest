module Admin
  class Counsel::QuestionCategoriesController < ApplicationController
    before_action :set_counsel_question_category, only: [:show, :edit, :update, :destroy]
    load_and_authorize_resource

    # GET /admin/counsel/question_categories
    # GET /admin/counsel/question_categories.json
    def index
      @counsel_question_categories = ::Counsel::QuestionCategory.all
    end

    # GET /admin/counsel/question_categories/1
    # GET /admin/counsel/question_categories/1.json
    def show
    end

    # GET /admin/counsel/question_categories/new
    def new
      @counsel_question_category = ::Counsel::QuestionCategory.new
    end

    # GET /admin/counsel/question_categories/1/edit
    def edit
    end

    # POST /admin/counsel/question_categories
    # POST /admin/counsel/question_categories.json
    def create
      @counsel_question_category = ::Counsel::QuestionCategory.new(counsel_question_category_params)

      respond_to do |format|
        if @counsel_question_category.save
          format.html { redirect_to admin_counsel_question_categories_url, notice: 'Question category was successfully created.' }
          format.json { render :show, status: :created, location: admin_counsel_question_categories_url }
        else
          format.html { render :new }
          format.json { render json: @counsel_question_category.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /admin/counsel/question_categories/1
    # PATCH/PUT /admin/counsel/question_categories/1.json
    def update
      respond_to do |format|
        if @counsel_question_category.update(counsel_question_category_params)
          format.html { redirect_to [:admin, @counsel_question_category], notice: 'Question category was successfully updated.' }
          format.json { render :show, status: :ok, location: [:admin, @counsel_question_category] }
        else
          format.html { render :edit }
          format.json { render json: @counsel_question_category.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /admin/counsel/question_categories/1
    # DELETE /admin/counsel/question_categories/1.json
    def destroy
      @counsel_question_category.destroy
      respond_to do |format|
        format.html { redirect_to admin_counsel_question_categories_url, notice: 'Question category was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_counsel_question_category
        @counsel_question_category = ::Counsel::QuestionCategory.where(id: params[:id]).first
        return invalid_access_flash_redirect_and_return if @counsel_question_category.blank?
      end

      # Only allow a trusted parameter "white list" through.
      def counsel_question_category_params
        default_values = {}
        params.require(:counsel_question_category).permit(:question_id, :category_id).merge(default_values)
      end
  end
end
