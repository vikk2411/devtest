module Admin
  class Counsel::QuestionsController < ApplicationController
    before_action :set_counsel_question, only: [:show, :edit, :update, :destroy]
    load_and_authorize_resource

    # GET /admin/counsel/questions
    # GET /admin/counsel/questions.json
    def index
      @counsel_questions = ::Counsel::Question.all
    end

    # GET /admin/counsel/questions/1
    # GET /admin/counsel/questions/1.json
    def show
    end

    # GET /admin/counsel/questions/new
    def new
      @counsel_question = ::Counsel::Question.new
    end

    # GET /admin/counsel/questions/1/edit
    def edit
    end

    # POST /admin/counsel/questions
    # POST /admin/counsel/questions.json
    def create
      @counsel_question = ::Counsel::Question.new(counsel_question_params)

      respond_to do |format|
        if @counsel_question.save
          format.html { redirect_to admin_counsel_questions_url, notice: 'Question was successfully created.' }
          format.json { render :show, status: :created, location: admin_counsel_questions_url }
        else
          format.html { render :new }
          format.json { render json: @counsel_question.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /admin/counsel/questions/1
    # PATCH/PUT /admin/counsel/questions/1.json
    def update
      respond_to do |format|
        if @counsel_question.update(counsel_question_params)
          format.html { redirect_to [:admin, @counsel_question], notice: 'Question was successfully updated.' }
          format.json { render :show, status: :ok, location: [:admin, @counsel_question] }
        else
          format.html { render :edit }
          format.json { render json: @counsel_question.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /admin/counsel/questions/1
    # DELETE /admin/counsel/questions/1.json
    def destroy
      @counsel_question.destroy
      respond_to do |format|
        format.html { redirect_to admin_counsel_questions_url, notice: 'Question was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_counsel_question
        @counsel_question = ::Counsel::Question.where(id: params[:id]).first
        return invalid_access_flash_redirect_and_return if @counsel_question.blank?
      end

      # Only allow a trusted parameter "white list" through.
      def counsel_question_params
        default_values = {}
        params.require(:counsel_question).permit(:question, :description, :asker_id, :asker_type).merge(default_values)
      end
  end
end
