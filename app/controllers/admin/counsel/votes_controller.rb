module Admin
  class Counsel::VotesController < ApplicationController
    before_action :set_counsel_vote, only: [:show, :edit, :update, :destroy]
    load_and_authorize_resource

    # GET /admin/counsel/votes
    # GET /admin/counsel/votes.json
    def index
      @counsel_votes = ::Counsel::Vote.all
    end

    # GET /admin/counsel/votes/1
    # GET /admin/counsel/votes/1.json
    def show
    end

    # GET /admin/counsel/votes/new
    def new
      @counsel_vote = ::Counsel::Vote.new
    end

    # GET /admin/counsel/votes/1/edit
    def edit
    end

    # POST /admin/counsel/votes
    # POST /admin/counsel/votes.json
    def create
      @counsel_vote = ::Counsel::Vote.new(counsel_vote_params)

      respond_to do |format|
        if @counsel_vote.save
          format.html { redirect_to admin_counsel_votes_url, notice: 'Vote was successfully created.' }
          format.json { render :show, status: :created, location: admin_counsel_votes_url }
        else
          format.html { render :new }
          format.json { render json: @counsel_vote.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /admin/counsel/votes/1
    # PATCH/PUT /admin/counsel/votes/1.json
    def update
      respond_to do |format|
        if @counsel_vote.update(counsel_vote_params)
          format.html { redirect_to [:admin, @counsel_vote], notice: 'Vote was successfully updated.' }
          format.json { render :show, status: :ok, location: [:admin, @counsel_vote] }
        else
          format.html { render :edit }
          format.json { render json: @counsel_vote.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /admin/counsel/votes/1
    # DELETE /admin/counsel/votes/1.json
    def destroy
      @counsel_vote.destroy
      respond_to do |format|
        format.html { redirect_to admin_counsel_votes_url, notice: 'Vote was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_counsel_vote
        @counsel_vote = ::Counsel::Vote.where(id: params[:id]).first
        return invalid_access_flash_redirect_and_return if @counsel_vote.blank?
      end

      # Only allow a trusted parameter "white list" through.
      def counsel_vote_params
        default_values = {}
        params.require(:counsel_vote).permit(:voter_id, :voter_type, :votable_id, :votable_type, :vote_type).merge(default_values)
      end
  end
end
