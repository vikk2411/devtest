module Admin
  class Counsel::CategoriesController < ApplicationController
    before_action :set_counsel_category, only: [:show, :edit, :update, :destroy]
    load_and_authorize_resource

    # GET /admin/counsel/categories
    # GET /admin/counsel/categories.json
    def index
      @counsel_categories = ::Counsel::Category.all
    end

    # GET /admin/counsel/categories/1
    # GET /admin/counsel/categories/1.json
    def show
    end

    # GET /admin/counsel/categories/new
    def new
      @counsel_category = ::Counsel::Category.new
    end

    # GET /admin/counsel/categories/1/edit
    def edit
    end

    # POST /admin/counsel/categories
    # POST /admin/counsel/categories.json
    def create
      @counsel_category = ::Counsel::Category.new(counsel_category_params)

      respond_to do |format|
        if @counsel_category.save
          format.html { redirect_to admin_counsel_categories_url, notice: 'Category was successfully created.' }
          format.json { render :show, status: :created, location: admin_counsel_categories_url }
        else
          format.html { render :new }
          format.json { render json: @counsel_category.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /admin/counsel/categories/1
    # PATCH/PUT /admin/counsel/categories/1.json
    def update
      respond_to do |format|
        if @counsel_category.update(counsel_category_params)
          format.html { redirect_to [:admin, @counsel_category], notice: 'Category was successfully updated.' }
          format.json { render :show, status: :ok, location: [:admin, @counsel_category] }
        else
          format.html { render :edit }
          format.json { render json: @counsel_category.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /admin/counsel/categories/1
    # DELETE /admin/counsel/categories/1.json
    def destroy
      @counsel_category.destroy
      respond_to do |format|
        format.html { redirect_to admin_counsel_categories_url, notice: 'Category was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_counsel_category
        @counsel_category = ::Counsel::Category.where(id: params[:id]).first
        return invalid_access_flash_redirect_and_return if @counsel_category.blank?
      end

      # Only allow a trusted parameter "white list" through.
      def counsel_category_params
        default_values = {}
        params.require(:counsel_category).permit(:name).merge(default_values)
      end
  end
end
