module Admin
  class ScopesController < ApplicationController
    before_action :set_scope, only: [:show, :edit, :update, :destroy]
    load_and_authorize_resource

    # GET /admin/scopes
    # GET /admin/scopes.json
    def index
      @scopes = Scope.all
    end

    # GET /admin/scopes/1
    # GET /admin/scopes/1.json
    def show
    end

    # GET /admin/scopes/new
    def new
      @scope = Scope.new
    end

    # GET /admin/scopes/1/edit
    def edit
    end

    # POST /admin/scopes
    # POST /admin/scopes.json
    def create
      @scope = Scope.new(scope_params)

      respond_to do |format|
        if @scope.save
          format.html { redirect_to admin_scopes_url, notice: 'Scope was successfully created.' }
          format.json { render :show, status: :created, location: admin_scopes_url }
        else
          format.html { render :new }
          format.json { render json: @scope.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /admin/scopes/1
    # PATCH/PUT /admin/scopes/1.json
    def update
      respond_to do |format|
        if @scope.update(scope_params)
          format.html { redirect_to [:admin, @scope], notice: 'Scope was successfully updated.' }
          format.json { render :show, status: :ok, location: [:admin, @scope] }
        else
          format.html { render :edit }
          format.json { render json: @scope.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /admin/scopes/1
    # DELETE /admin/scopes/1.json
    def destroy
      @scope.destroy
      respond_to do |format|
        format.html { redirect_to admin_scopes_url, notice: 'Scope was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_scope
        @scope = Scope.where(id: params[:id]).first
        return invalid_access_flash_redirect_and_return if @scope.blank?
      end

      # Only allow a trusted parameter "white list" through.
      def scope_params
        default_values = {}
        params.require(:scope).permit(:name, :user_process_id, :professional_id, :inclusions, :exclusions, :documents, :deliverables, :status, :started_at, :closed_at, :expected_duration).merge(default_values)
      end
  end
end
