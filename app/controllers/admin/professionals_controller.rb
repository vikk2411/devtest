module Admin
  class ProfessionalsController < ApplicationController
    before_action :set_professional, only: [:show, :edit, :update, :destroy]
    load_and_authorize_resource

    # GET /admin/professionals
    # GET /admin/professionals.json
    def index
      @professionals = Professional.all
    end

    # GET /admin/professionals/1
    # GET /admin/professionals/1.json
    def show
    end

    # GET /admin/professionals/new
    def new
      @professional = Professional.new
    end

    # GET /admin/professionals/1/edit
    def edit
    end

    # POST /admin/professionals
    # POST /admin/professionals.json
    def create
      @professional = Professional.new(professional_params)

      respond_to do |format|
        if @professional.save
          format.html { redirect_to admin_professionals_url, notice: 'Professional was successfully created.' }
          format.json { render :show, status: :created, location: admin_professionals_url }
        else
          format.html { render :new }
          format.json { render json: @professional.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /admin/professionals/1
    # PATCH/PUT /admin/professionals/1.json
    def update
      respond_to do |format|
        if @professional.update(professional_params)
          format.html { redirect_to [:admin, @professional], notice: 'Professional was successfully updated.' }
          format.json { render :show, status: :ok, location: [:admin, @professional] }
        else
          format.html { render :edit }
          format.json { render json: @professional.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /admin/professionals/1
    # DELETE /admin/professionals/1.json
    def destroy
      @professional.destroy
      respond_to do |format|
        format.html { redirect_to admin_professionals_url, notice: 'Professional was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_professional
        @professional = Professional.where(id: params[:id]).first
        return invalid_access_flash_redirect_and_return if @professional.blank?
      end

      # Only allow a trusted parameter "white list" through.
      def professional_params
        default_values = {}
        params.require(:professional).permit(:id_token, :user_id, :experience_start_at, :source, :company_name, :location, :photo, :about, :education, :experience, :clients, :aggregate_rating, :status, :professional_type, :other_details, :consult_rate, :platform_fee).merge(default_values)
      end
  end
end
