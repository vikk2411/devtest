module Admin
  class GlobalSettingsController < ApplicationController
    before_action :set_global_setting, only: [:show, :edit, :update, :destroy]
    load_and_authorize_resource

    # GET /admin/global_settings
    # GET /admin/global_settings.json
    def index
      @global_settings = GlobalSetting.all
    end

    # GET /admin/global_settings/1
    # GET /admin/global_settings/1.json
    def show
    end

    # GET /admin/global_settings/new
    def new
      @global_setting = GlobalSetting.new
    end

    # GET /admin/global_settings/1/edit
    def edit
    end

    # POST /admin/global_settings
    # POST /admin/global_settings.json
    def create
      @global_setting = GlobalSetting.new(global_setting_params)

      respond_to do |format|
        if @global_setting.save
          format.html { redirect_to admin_global_settings_url, notice: 'Global setting was successfully created.' }
          format.json { render :show, status: :created, location: admin_global_settings_url }
        else
          format.html { render :new }
          format.json { render json: @global_setting.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /admin/global_settings/1
    # PATCH/PUT /admin/global_settings/1.json
    def update
      respond_to do |format|
        if @global_setting.update(global_setting_params)
          format.html { redirect_to [:admin, @global_setting], notice: 'Global setting was successfully updated.' }
          format.json { render :show, status: :ok, location: [:admin, @global_setting] }
        else
          format.html { render :edit }
          format.json { render json: @global_setting.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /admin/global_settings/1
    # DELETE /admin/global_settings/1.json
    def destroy
      @global_setting.destroy
      respond_to do |format|
        format.html { redirect_to admin_global_settings_url, notice: 'Global setting was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_global_setting
        @global_setting = GlobalSetting.where(id: params[:id]).first
        return invalid_access_flash_redirect_and_return if @global_setting.blank?
      end

      # Only allow a trusted parameter "white list" through.
      def global_setting_params
        default_values = {}
        params.require(:global_setting).permit(:settings).merge(default_values)
      end
  end
end
