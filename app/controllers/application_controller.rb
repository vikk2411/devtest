require 'ruby-prof' if Rails.env.development?
class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  around_action :code_profile if Rails.env.development?

  def code_profile
    if params[:code_profile]
      result = RubyProf.profile { yield }
      if result
        out = StringIO.new
        RubyProf::GraphHtmlPrinter.new(result).print out, min_percent: 0
        self.response_body = out.string
      end
    else
      yield
    end
  end

  def redirect_to_back_or_default(default = nil)
    redirect_back(fallback_location: default || root_url)
  end

  def xhr?
    requested_with = request.headers['HTTP_X_REQUESTED_WITH'].to_s
    return requested_with.casecmp('XMLHttpRequest').zero?
  end

  def invalid_access_flash_redirect_and_return(error_message = nil)
    if not xhr?
      flash[:error] = error_message || 'You have ventured into uncharted territories'
      redirect_to_back_or_default
    else
      render text: error_message || 'Invalid Access', status: 400
    end
    return false
  end

  def after_sign_in_path_for(resource)
    if resource.is_a?(User)
      stored_location_for(resource) || user_dashboard_path
    elsif resource.is_a?(AdminUser)
      stored_location_for(resource) || admin_dashboard_path
    else
      stored_location_for(resource) || home_path
    end
  end

  def root_url
    if user_signed_in?
      user_dashboard_url
    elsif admin_user_signed_in?
      admin_dashboard_url
    else
      home_url
    end
  end

  def current_ability
    if user_signed_in?
      @current_ability ||= Ability.new(current_user)
    elsif admin_user_signed_in?
      @current_ability ||= Ability.new(current_admin_user)
    else
      @current_ability ||= Ability.new(current_user)
    end
  end

  rescue_from CanCan::AccessDenied do |exception|
    invalid_access_flash_redirect_and_return exception.message
  end
end
