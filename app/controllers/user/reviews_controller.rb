module User
  class ReviewsController < ApplicationController
    before_action :set_review, only: [:show, :edit, :update, :destroy]
    load_and_authorize_resource

    # GET /user/reviews
    # GET /user/reviews.json
    def index
      @reviews = Review.where(user_id: current_user.id)
    end

    # GET /user/reviews/1
    # GET /user/reviews/1.json
    def show
    end

    # GET /user/reviews/new
    def new
      @review = Review.new
    end

    # GET /user/reviews/1/edit
    def edit
    end

    # POST /user/reviews
    # POST /user/reviews.json
    def create
      @review = Review.new(review_params.merge(user: current_user))
      respond_to do |format|
        if @review.save
          format.html { redirect_to user_reviews_url, notice: 'Review was successfully created.' }
          format.json { render :show, status: :created, location: user_reviews_url }
        else
          format.html { render :new }
          format.json { render json: @review.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /user/reviews/1
    # PATCH/PUT /user/reviews/1.json
    def update
      respond_to do |format|
        if @review.update(review_params)
          format.html { redirect_to [:user, @review], notice: 'Review was successfully updated.' }
          format.json { render :show, status: :ok, location: [:user, @review] }
        else
          format.html { render :edit }
          format.json { render json: @review.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /user/reviews/1
    # DELETE /user/reviews/1.json
    def destroy
      @review.destroy
      respond_to do |format|
        format.html { redirect_to user_reviews_url, notice: 'Review was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_review
        @review = Review.where(id: params[:id], user_id: current_user.id).first
        return invalid_access_flash_redirect_and_return if @review.blank?
      end

      # Only allow a trusted parameter "white list" through.
      def review_params
        default_values = {user_id: current_user.id}
        params.require(:review).permit(:professional_id, :rating, :review, :user_process_id).merge(default_values)
      end
  end
end
