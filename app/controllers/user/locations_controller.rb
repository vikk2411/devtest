module User
  class LocationsController < ApplicationController
    before_action :set_location, only: [:show, :edit, :update, :destroy]
    load_and_authorize_resource

    # GET /user/locations
    # GET /user/locations.json
    def index
      @locations = Location.where(user_id: current_user.id)
    end

    # GET /user/locations/1
    # GET /user/locations/1.json
    def show
    end

    # GET /user/locations/new
    def new
      @location = Location.new
    end

    # GET /user/locations/1/edit
    def edit
    end

    # POST /user/locations
    # POST /user/locations.json
    def create
      @location = Location.new(location_params)
      respond_to do |format|
        if @location.save
          format.html { redirect_to user_locations_url, notice: 'Location was successfully created.' }
          format.json { render :show, status: :created, location: user_locations_url }
        else
          format.html { render :new }
          format.json { render json: @location.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /user/locations/1
    # PATCH/PUT /user/locations/1.json
    def update
      respond_to do |format|
        if @location.update(location_params)
          format.html { redirect_to [:user, @location], notice: 'Location was successfully updated.' }
          format.json { render :show, status: :ok, location: [:user, @location] }
        else
          format.html { render :edit }
          format.json { render json: @location.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /user/locations/1
    # DELETE /user/locations/1.json
    def destroy
      @location.destroy
      respond_to do |format|
        format.html { redirect_to user_locations_url, notice: 'Location was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_location
        @location = Location.where(id: params[:id], user_id: current_user.id).first
        return invalid_access_flash_redirect_and_return if @location.blank?
      end

      # Only allow a trusted parameter "white list" through.
      def location_params
        default_values = {user_id: current_user.id}
        params.require(:location).permit(:name).merge(default_values)
      end
  end
end
