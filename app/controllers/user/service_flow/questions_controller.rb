module User
  class ServiceFlow::QuestionsController < ApplicationController
    before_action :set_service_flow_question, only: [:show, :edit, :update, :destroy]
    load_and_authorize_resource

    # GET /user/service_flow/questions
    # GET /user/service_flow/questions.json
    def index
      @service_flow_questions = ::ServiceFlow::Question.where(user_id: current_user.id)
    end

    # GET /user/service_flow/questions/1
    # GET /user/service_flow/questions/1.json
    def show
    end

    # GET /user/service_flow/questions/new
    def new
      @service_flow_question = ::ServiceFlow::Question.new
    end

    # GET /user/service_flow/questions/1/edit
    def edit
    end

    # POST /user/service_flow/questions
    # POST /user/service_flow/questions.json
    def create
      @service_flow_question = ::ServiceFlow::Question.new(service_flow_question_params)
      respond_to do |format|
        if @service_flow_question.save
          format.html { redirect_to user_service_flow_questions_url, notice: 'Question was successfully created.' }
          format.json { render :show, status: :created, location: user_service_flow_questions_url }
        else
          format.html { render :new }
          format.json { render json: @service_flow_question.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /user/service_flow/questions/1
    # PATCH/PUT /user/service_flow/questions/1.json
    def update
      respond_to do |format|
        if @service_flow_question.update(service_flow_question_params)
          format.html { redirect_to [:user, @service_flow_question], notice: 'Question was successfully updated.' }
          format.json { render :show, status: :ok, location: [:user, @service_flow_question] }
        else
          format.html { render :edit }
          format.json { render json: @service_flow_question.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /user/service_flow/questions/1
    # DELETE /user/service_flow/questions/1.json
    def destroy
      @service_flow_question.destroy
      respond_to do |format|
        format.html { redirect_to user_service_flow_questions_url, notice: 'Question was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_service_flow_question
        @service_flow_question = ::ServiceFlow::Question.where(id: params[:id], user_id: current_user.id).first
        return invalid_access_flash_redirect_and_return if @service_flow_question.blank?
      end

      # Only allow a trusted parameter "white list" through.
      def service_flow_question_params
        default_values = {user_id: current_user.id}
        params.require(:service_flow_question).permit(:question, :question_type, :parent).merge(default_values)
      end
  end
end
