module User
  class ServiceFlow::UserAnswerChoicesController < ApplicationController
    before_action :set_service_flow_user_answer_choice, only: [:show, :edit, :update, :destroy]
    load_and_authorize_resource

    # GET /user/service_flow/user_answer_choices
    # GET /user/service_flow/user_answer_choices.json
    def index
      @service_flow_user_answer_choices = ::ServiceFlow::UserAnswerChoice.where(user_id: current_user.id)
    end

    # GET /user/service_flow/user_answer_choices/1
    # GET /user/service_flow/user_answer_choices/1.json
    def show
    end

    # GET /user/service_flow/user_answer_choices/new
    def new
      @service_flow_user_answer_choice = ::ServiceFlow::UserAnswerChoice.new
    end

    # GET /user/service_flow/user_answer_choices/1/edit
    def edit
    end

    # POST /user/service_flow/user_answer_choices
    # POST /user/service_flow/user_answer_choices.json
    def create
      @service_flow_user_answer_choice = ::ServiceFlow::UserAnswerChoice.new(service_flow_user_answer_choice_params.merge(user: current_user))
      respond_to do |format|
        if @service_flow_user_answer_choice.save
          format.html { redirect_to user_service_flow_user_answer_choices_url, notice: 'User answer choice was successfully created.' }
          format.json { render :show, status: :created, location: user_service_flow_user_answer_choices_url }
        else
          format.html { render :new }
          format.json { render json: @service_flow_user_answer_choice.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /user/service_flow/user_answer_choices/1
    # PATCH/PUT /user/service_flow/user_answer_choices/1.json
    def update
      respond_to do |format|
        if @service_flow_user_answer_choice.update(service_flow_user_answer_choice_params)
          format.html { redirect_to [:user, @service_flow_user_answer_choice], notice: 'User answer choice was successfully updated.' }
          format.json { render :show, status: :ok, location: [:user, @service_flow_user_answer_choice] }
        else
          format.html { render :edit }
          format.json { render json: @service_flow_user_answer_choice.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /user/service_flow/user_answer_choices/1
    # DELETE /user/service_flow/user_answer_choices/1.json
    def destroy
      @service_flow_user_answer_choice.destroy
      respond_to do |format|
        format.html { redirect_to user_service_flow_user_answer_choices_url, notice: 'User answer choice was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_service_flow_user_answer_choice
        @service_flow_user_answer_choice = ::ServiceFlow::UserAnswerChoice.where(id: params[:id], user_id: current_user.id).first
        return invalid_access_flash_redirect_and_return if @service_flow_user_answer_choice.blank?
      end

      # Only allow a trusted parameter "white list" through.
      def service_flow_user_answer_choice_params
        default_values = {user_id: current_user.id}
        params.require(:service_flow_user_answer_choice).permit(:answer_choice_id, :user_process_id).merge(default_values)
      end
  end
end
