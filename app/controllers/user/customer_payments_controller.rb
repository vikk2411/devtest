module User
  class CustomerPaymentsController < ApplicationController
    before_action :set_customer_payment, only: [:show, :edit, :update, :destroy]
    load_and_authorize_resource

    # GET /user/customer_payments
    # GET /user/customer_payments.json
    def index
      @customer_payments = CustomerPayment.where(user_id: current_user.id)
    end

    # GET /user/customer_payments/1
    # GET /user/customer_payments/1.json
    def show
    end

    # GET /user/customer_payments/new
    def new
      @customer_payment = CustomerPayment.new
    end

    # GET /user/customer_payments/1/edit
    def edit
    end

    # POST /user/customer_payments
    # POST /user/customer_payments.json
    def create
      @customer_payment = CustomerPayment.new(customer_payment_params.merge(user: current_user))
      respond_to do |format|
        if @customer_payment.save
          format.html { redirect_to user_customer_payments_url, notice: 'Customer payment was successfully created.' }
          format.json { render :show, status: :created, location: user_customer_payments_url }
        else
          format.html { render :new }
          format.json { render json: @customer_payment.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /user/customer_payments/1
    # PATCH/PUT /user/customer_payments/1.json
    def update
      respond_to do |format|
        if @customer_payment.update(customer_payment_params)
          format.html { redirect_to [:user, @customer_payment], notice: 'Customer payment was successfully updated.' }
          format.json { render :show, status: :ok, location: [:user, @customer_payment] }
        else
          format.html { render :edit }
          format.json { render json: @customer_payment.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /user/customer_payments/1
    # DELETE /user/customer_payments/1.json
    def destroy
      @customer_payment.destroy
      respond_to do |format|
        format.html { redirect_to user_customer_payments_url, notice: 'Customer payment was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_customer_payment
        @customer_payment = CustomerPayment.where(id: params[:id], user_id: current_user.id).first
        return invalid_access_flash_redirect_and_return if @customer_payment.blank?
      end

      # Only allow a trusted parameter "white list" through.
      def customer_payment_params
        default_values = {user_id: current_user.id}
        params.require(:customer_payment).permit(:id_token, :professional_id, :amount, :paid_amount, :payable_id, :payable_type, :status, :user_process_id, :scope_id).merge(default_values)
      end
  end
end
