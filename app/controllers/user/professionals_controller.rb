module User
  class ProfessionalsController < ApplicationController
    before_action :set_professional, only: [:show, :edit, :update, :destroy]
    load_and_authorize_resource

    # GET /user/professionals
    # GET /user/professionals.json
    def index
      @professionals = Professional.where(user_id: current_user.id)
    end

    # GET /user/professionals/1
    # GET /user/professionals/1.json
    def show
    end

    # GET /user/professionals/new
    def new
      @professional = Professional.new
    end

    # GET /user/professionals/1/edit
    def edit
    end

    # POST /user/professionals
    # POST /user/professionals.json
    def create
      @professional = Professional.new(professional_params.merge(user: current_user))
      respond_to do |format|
        if @professional.save
          format.html { redirect_to user_professionals_url, notice: 'Professional was successfully created.' }
          format.json { render :show, status: :created, location: user_professionals_url }
        else
          format.html { render :new }
          format.json { render json: @professional.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /user/professionals/1
    # PATCH/PUT /user/professionals/1.json
    def update
      respond_to do |format|
        if @professional.update(professional_params)
          format.html { redirect_to [:user, @professional], notice: 'Professional was successfully updated.' }
          format.json { render :show, status: :ok, location: [:user, @professional] }
        else
          format.html { render :edit }
          format.json { render json: @professional.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /user/professionals/1
    # DELETE /user/professionals/1.json
    def destroy
      @professional.destroy
      respond_to do |format|
        format.html { redirect_to user_professionals_url, notice: 'Professional was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_professional
        @professional = Professional.where(id: params[:id], user_id: current_user.id).first
        return invalid_access_flash_redirect_and_return if @professional.blank?
      end

      # Only allow a trusted parameter "white list" through.
      def professional_params
        default_values = {user_id: current_user.id}
        params.require(:professional).permit(:id_token, :experience_start_at, :source, :company_name, :location, :photo, :about, :education, :experience, :clients, :aggregate_rating, :status, :professional_type, :other_details, :consult_rate, :platform_fee).merge(default_values)
      end
  end
end
