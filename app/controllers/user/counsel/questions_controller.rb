module User
  class Counsel::QuestionsController < ApplicationController
    before_action :set_counsel_question, only: [:show, :edit, :update, :destroy]
    load_and_authorize_resource

    # GET /user/counsel/questions
    # GET /user/counsel/questions.json
    def index
      @counsel_questions = ::Counsel::Question.where(user_id: current_user.id)
    end

    # GET /user/counsel/questions/1
    # GET /user/counsel/questions/1.json
    def show
    end

    # GET /user/counsel/questions/new
    def new
      @counsel_question = ::Counsel::Question.new
    end

    # GET /user/counsel/questions/1/edit
    def edit
    end

    # POST /user/counsel/questions
    # POST /user/counsel/questions.json
    def create
      @counsel_question = ::Counsel::Question.new(counsel_question_params)
      respond_to do |format|
        if @counsel_question.save
          format.html { redirect_to user_counsel_questions_url, notice: 'Question was successfully created.' }
          format.json { render :show, status: :created, location: user_counsel_questions_url }
        else
          format.html { render :new }
          format.json { render json: @counsel_question.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /user/counsel/questions/1
    # PATCH/PUT /user/counsel/questions/1.json
    def update
      respond_to do |format|
        if @counsel_question.update(counsel_question_params)
          format.html { redirect_to [:user, @counsel_question], notice: 'Question was successfully updated.' }
          format.json { render :show, status: :ok, location: [:user, @counsel_question] }
        else
          format.html { render :edit }
          format.json { render json: @counsel_question.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /user/counsel/questions/1
    # DELETE /user/counsel/questions/1.json
    def destroy
      @counsel_question.destroy
      respond_to do |format|
        format.html { redirect_to user_counsel_questions_url, notice: 'Question was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_counsel_question
        @counsel_question = ::Counsel::Question.where(id: params[:id], user_id: current_user.id).first
        return invalid_access_flash_redirect_and_return if @counsel_question.blank?
      end

      # Only allow a trusted parameter "white list" through.
      def counsel_question_params
        default_values = {user_id: current_user.id}
        params.require(:counsel_question).permit(:question, :description, :asker_id, :asker_type).merge(default_values)
      end
  end
end
