module User
  class Counsel::QuestionCategoriesController < ApplicationController
    before_action :set_counsel_question_category, only: [:show, :edit, :update, :destroy]
    load_and_authorize_resource

    # GET /user/counsel/question_categories
    # GET /user/counsel/question_categories.json
    def index
      @counsel_question_categories = ::Counsel::QuestionCategory.where(user_id: current_user.id)
    end

    # GET /user/counsel/question_categories/1
    # GET /user/counsel/question_categories/1.json
    def show
    end

    # GET /user/counsel/question_categories/new
    def new
      @counsel_question_category = ::Counsel::QuestionCategory.new
    end

    # GET /user/counsel/question_categories/1/edit
    def edit
    end

    # POST /user/counsel/question_categories
    # POST /user/counsel/question_categories.json
    def create
      @counsel_question_category = ::Counsel::QuestionCategory.new(counsel_question_category_params)
      respond_to do |format|
        if @counsel_question_category.save
          format.html { redirect_to user_counsel_question_categories_url, notice: 'Question category was successfully created.' }
          format.json { render :show, status: :created, location: user_counsel_question_categories_url }
        else
          format.html { render :new }
          format.json { render json: @counsel_question_category.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /user/counsel/question_categories/1
    # PATCH/PUT /user/counsel/question_categories/1.json
    def update
      respond_to do |format|
        if @counsel_question_category.update(counsel_question_category_params)
          format.html { redirect_to [:user, @counsel_question_category], notice: 'Question category was successfully updated.' }
          format.json { render :show, status: :ok, location: [:user, @counsel_question_category] }
        else
          format.html { render :edit }
          format.json { render json: @counsel_question_category.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /user/counsel/question_categories/1
    # DELETE /user/counsel/question_categories/1.json
    def destroy
      @counsel_question_category.destroy
      respond_to do |format|
        format.html { redirect_to user_counsel_question_categories_url, notice: 'Question category was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_counsel_question_category
        @counsel_question_category = ::Counsel::QuestionCategory.where(id: params[:id], user_id: current_user.id).first
        return invalid_access_flash_redirect_and_return if @counsel_question_category.blank?
      end

      # Only allow a trusted parameter "white list" through.
      def counsel_question_category_params
        default_values = {user_id: current_user.id}
        params.require(:counsel_question_category).permit(:question_id, :category_id).merge(default_values)
      end
  end
end
