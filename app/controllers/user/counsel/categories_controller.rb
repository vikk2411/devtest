module User
  class Counsel::CategoriesController < ApplicationController
    before_action :set_counsel_category, only: [:show, :edit, :update, :destroy]
    load_and_authorize_resource

    # GET /user/counsel/categories
    # GET /user/counsel/categories.json
    def index
      @counsel_categories = ::Counsel::Category.where(user_id: current_user.id)
    end

    # GET /user/counsel/categories/1
    # GET /user/counsel/categories/1.json
    def show
    end

    # GET /user/counsel/categories/new
    def new
      @counsel_category = ::Counsel::Category.new
    end

    # GET /user/counsel/categories/1/edit
    def edit
    end

    # POST /user/counsel/categories
    # POST /user/counsel/categories.json
    def create
      @counsel_category = ::Counsel::Category.new(counsel_category_params)
      respond_to do |format|
        if @counsel_category.save
          format.html { redirect_to user_counsel_categories_url, notice: 'Category was successfully created.' }
          format.json { render :show, status: :created, location: user_counsel_categories_url }
        else
          format.html { render :new }
          format.json { render json: @counsel_category.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /user/counsel/categories/1
    # PATCH/PUT /user/counsel/categories/1.json
    def update
      respond_to do |format|
        if @counsel_category.update(counsel_category_params)
          format.html { redirect_to [:user, @counsel_category], notice: 'Category was successfully updated.' }
          format.json { render :show, status: :ok, location: [:user, @counsel_category] }
        else
          format.html { render :edit }
          format.json { render json: @counsel_category.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /user/counsel/categories/1
    # DELETE /user/counsel/categories/1.json
    def destroy
      @counsel_category.destroy
      respond_to do |format|
        format.html { redirect_to user_counsel_categories_url, notice: 'Category was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_counsel_category
        @counsel_category = ::Counsel::Category.where(id: params[:id], user_id: current_user.id).first
        return invalid_access_flash_redirect_and_return if @counsel_category.blank?
      end

      # Only allow a trusted parameter "white list" through.
      def counsel_category_params
        default_values = {user_id: current_user.id}
        params.require(:counsel_category).permit(:name).merge(default_values)
      end
  end
end
