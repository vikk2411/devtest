module User
  class Counsel::AnswersController < ApplicationController
    before_action :set_counsel_answer, only: [:show, :edit, :update, :destroy]
    load_and_authorize_resource

    # GET /user/counsel/answers
    # GET /user/counsel/answers.json
    def index
      @counsel_answers = ::Counsel::Answer.where(user_id: current_user.id)
    end

    # GET /user/counsel/answers/1
    # GET /user/counsel/answers/1.json
    def show
    end

    # GET /user/counsel/answers/new
    def new
      @counsel_answer = ::Counsel::Answer.new
    end

    # GET /user/counsel/answers/1/edit
    def edit
    end

    # POST /user/counsel/answers
    # POST /user/counsel/answers.json
    def create
      @counsel_answer = ::Counsel::Answer.new(counsel_answer_params)
      respond_to do |format|
        if @counsel_answer.save
          format.html { redirect_to user_counsel_answers_url, notice: 'Answer was successfully created.' }
          format.json { render :show, status: :created, location: user_counsel_answers_url }
        else
          format.html { render :new }
          format.json { render json: @counsel_answer.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /user/counsel/answers/1
    # PATCH/PUT /user/counsel/answers/1.json
    def update
      respond_to do |format|
        if @counsel_answer.update(counsel_answer_params)
          format.html { redirect_to [:user, @counsel_answer], notice: 'Answer was successfully updated.' }
          format.json { render :show, status: :ok, location: [:user, @counsel_answer] }
        else
          format.html { render :edit }
          format.json { render json: @counsel_answer.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /user/counsel/answers/1
    # DELETE /user/counsel/answers/1.json
    def destroy
      @counsel_answer.destroy
      respond_to do |format|
        format.html { redirect_to user_counsel_answers_url, notice: 'Answer was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_counsel_answer
        @counsel_answer = ::Counsel::Answer.where(id: params[:id], user_id: current_user.id).first
        return invalid_access_flash_redirect_and_return if @counsel_answer.blank?
      end

      # Only allow a trusted parameter "white list" through.
      def counsel_answer_params
        default_values = {user_id: current_user.id}
        params.require(:counsel_answer).permit(:question_id, :answer, :answerer_id, :answerer_type, :accepted, :deleted).merge(default_values)
      end
  end
end
