module User
  class Counsel::AnswerRequestsController < ApplicationController
    before_action :set_counsel_answer_request, only: [:show, :edit, :update, :destroy]
    load_and_authorize_resource

    # GET /user/counsel/answer_requests
    # GET /user/counsel/answer_requests.json
    def index
      @counsel_answer_requests = ::Counsel::AnswerRequest.where(user_id: current_user.id)
    end

    # GET /user/counsel/answer_requests/1
    # GET /user/counsel/answer_requests/1.json
    def show
    end

    # GET /user/counsel/answer_requests/new
    def new
      @counsel_answer_request = ::Counsel::AnswerRequest.new
    end

    # GET /user/counsel/answer_requests/1/edit
    def edit
    end

    # POST /user/counsel/answer_requests
    # POST /user/counsel/answer_requests.json
    def create
      @counsel_answer_request = ::Counsel::AnswerRequest.new(counsel_answer_request_params)
      respond_to do |format|
        if @counsel_answer_request.save
          format.html { redirect_to user_counsel_answer_requests_url, notice: 'Answer request was successfully created.' }
          format.json { render :show, status: :created, location: user_counsel_answer_requests_url }
        else
          format.html { render :new }
          format.json { render json: @counsel_answer_request.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /user/counsel/answer_requests/1
    # PATCH/PUT /user/counsel/answer_requests/1.json
    def update
      respond_to do |format|
        if @counsel_answer_request.update(counsel_answer_request_params)
          format.html { redirect_to [:user, @counsel_answer_request], notice: 'Answer request was successfully updated.' }
          format.json { render :show, status: :ok, location: [:user, @counsel_answer_request] }
        else
          format.html { render :edit }
          format.json { render json: @counsel_answer_request.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /user/counsel/answer_requests/1
    # DELETE /user/counsel/answer_requests/1.json
    def destroy
      @counsel_answer_request.destroy
      respond_to do |format|
        format.html { redirect_to user_counsel_answer_requests_url, notice: 'Answer request was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_counsel_answer_request
        @counsel_answer_request = ::Counsel::AnswerRequest.where(id: params[:id], user_id: current_user.id).first
        return invalid_access_flash_redirect_and_return if @counsel_answer_request.blank?
      end

      # Only allow a trusted parameter "white list" through.
      def counsel_answer_request_params
        default_values = {user_id: current_user.id}
        params.require(:counsel_answer_request).permit(:question_id, :requester_id, :requester_type, :answerer_id, :answerer_type).merge(default_values)
      end
  end
end
