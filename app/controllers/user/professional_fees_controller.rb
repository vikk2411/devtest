module User
  class ProfessionalFeesController < ApplicationController
    before_action :set_professional_fee, only: [:show, :edit, :update, :destroy]
    load_and_authorize_resource

    # GET /user/professional_fees
    # GET /user/professional_fees.json
    def index
      @professional_fees = ProfessionalFee.where(user_id: current_user.id)
    end

    # GET /user/professional_fees/1
    # GET /user/professional_fees/1.json
    def show
    end

    # GET /user/professional_fees/new
    def new
      @professional_fee = ProfessionalFee.new
    end

    # GET /user/professional_fees/1/edit
    def edit
    end

    # POST /user/professional_fees
    # POST /user/professional_fees.json
    def create
      @professional_fee = ProfessionalFee.new(professional_fee_params)
      respond_to do |format|
        if @professional_fee.save
          format.html { redirect_to user_professional_fees_url, notice: 'Professional fee was successfully created.' }
          format.json { render :show, status: :created, location: user_professional_fees_url }
        else
          format.html { render :new }
          format.json { render json: @professional_fee.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /user/professional_fees/1
    # PATCH/PUT /user/professional_fees/1.json
    def update
      respond_to do |format|
        if @professional_fee.update(professional_fee_params)
          format.html { redirect_to [:user, @professional_fee], notice: 'Professional fee was successfully updated.' }
          format.json { render :show, status: :ok, location: [:user, @professional_fee] }
        else
          format.html { render :edit }
          format.json { render json: @professional_fee.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /user/professional_fees/1
    # DELETE /user/professional_fees/1.json
    def destroy
      @professional_fee.destroy
      respond_to do |format|
        format.html { redirect_to user_professional_fees_url, notice: 'Professional fee was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_professional_fee
        @professional_fee = ProfessionalFee.where(id: params[:id], user_id: current_user.id).first
        return invalid_access_flash_redirect_and_return if @professional_fee.blank?
      end

      # Only allow a trusted parameter "white list" through.
      def professional_fee_params
        default_values = {user_id: current_user.id}
        params.require(:professional_fee).permit(:professional_id, :scenario_id, :location_id, :status, :professional_fee, :government_fee, :oope, :service_tax, :tax).merge(default_values)
      end
  end
end
