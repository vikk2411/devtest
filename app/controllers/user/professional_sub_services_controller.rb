module User
  class ProfessionalSubServicesController < ApplicationController
    before_action :set_professional_sub_service, only: [:show, :edit, :update, :destroy]
    load_and_authorize_resource

    # GET /user/professional_sub_services
    # GET /user/professional_sub_services.json
    def index
      @professional_sub_services = ProfessionalSubService.where(user_id: current_user.id)
    end

    # GET /user/professional_sub_services/1
    # GET /user/professional_sub_services/1.json
    def show
    end

    # GET /user/professional_sub_services/new
    def new
      @professional_sub_service = ProfessionalSubService.new
    end

    # GET /user/professional_sub_services/1/edit
    def edit
    end

    # POST /user/professional_sub_services
    # POST /user/professional_sub_services.json
    def create
      @professional_sub_service = ProfessionalSubService.new(professional_sub_service_params)
      respond_to do |format|
        if @professional_sub_service.save
          format.html { redirect_to user_professional_sub_services_url, notice: 'Professional sub service was successfully created.' }
          format.json { render :show, status: :created, location: user_professional_sub_services_url }
        else
          format.html { render :new }
          format.json { render json: @professional_sub_service.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /user/professional_sub_services/1
    # PATCH/PUT /user/professional_sub_services/1.json
    def update
      respond_to do |format|
        if @professional_sub_service.update(professional_sub_service_params)
          format.html { redirect_to [:user, @professional_sub_service], notice: 'Professional sub service was successfully updated.' }
          format.json { render :show, status: :ok, location: [:user, @professional_sub_service] }
        else
          format.html { render :edit }
          format.json { render json: @professional_sub_service.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /user/professional_sub_services/1
    # DELETE /user/professional_sub_services/1.json
    def destroy
      @professional_sub_service.destroy
      respond_to do |format|
        format.html { redirect_to user_professional_sub_services_url, notice: 'Professional sub service was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_professional_sub_service
        @professional_sub_service = ProfessionalSubService.where(id: params[:id], user_id: current_user.id).first
        return invalid_access_flash_redirect_and_return if @professional_sub_service.blank?
      end

      # Only allow a trusted parameter "white list" through.
      def professional_sub_service_params
        default_values = {user_id: current_user.id}
        params.require(:professional_sub_service).permit(:professional_id, :sub_service_id, :professional_service_id).merge(default_values)
      end
  end
end
