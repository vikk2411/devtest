module User
  class ScenariosController < ApplicationController
    before_action :set_scenario, only: [:show, :edit, :update, :destroy]
    load_and_authorize_resource

    # GET /user/scenarios
    # GET /user/scenarios.json
    def index
      @scenarios = Scenario.where(user_id: current_user.id)
    end

    # GET /user/scenarios/1
    # GET /user/scenarios/1.json
    def show
    end

    # GET /user/scenarios/new
    def new
      @scenario = Scenario.new
    end

    # GET /user/scenarios/1/edit
    def edit
    end

    # POST /user/scenarios
    # POST /user/scenarios.json
    def create
      @scenario = Scenario.new(scenario_params)
      respond_to do |format|
        if @scenario.save
          format.html { redirect_to user_scenarios_url, notice: 'Scenario was successfully created.' }
          format.json { render :show, status: :created, location: user_scenarios_url }
        else
          format.html { render :new }
          format.json { render json: @scenario.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /user/scenarios/1
    # PATCH/PUT /user/scenarios/1.json
    def update
      respond_to do |format|
        if @scenario.update(scenario_params)
          format.html { redirect_to [:user, @scenario], notice: 'Scenario was successfully updated.' }
          format.json { render :show, status: :ok, location: [:user, @scenario] }
        else
          format.html { render :edit }
          format.json { render json: @scenario.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /user/scenarios/1
    # DELETE /user/scenarios/1.json
    def destroy
      @scenario.destroy
      respond_to do |format|
        format.html { redirect_to user_scenarios_url, notice: 'Scenario was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_scenario
        @scenario = Scenario.where(id: params[:id], user_id: current_user.id).first
        return invalid_access_flash_redirect_and_return if @scenario.blank?
      end

      # Only allow a trusted parameter "white list" through.
      def scenario_params
        default_values = {user_id: current_user.id}
        params.fetch(:scenario, {})
      end
  end
end
