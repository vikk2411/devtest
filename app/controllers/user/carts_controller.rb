module User
  class CartsController < ApplicationController
    before_action :set_cart, only: [:show, :edit, :update, :destroy]
    load_and_authorize_resource

    # GET /user/carts
    # GET /user/carts.json
    def index
      @carts = Cart.where(user_id: current_user.id)
    end

    # GET /user/carts/1
    # GET /user/carts/1.json
    def show
    end

    # GET /user/carts/new
    def new
      @cart = Cart.new
    end

    # GET /user/carts/1/edit
    def edit
    end

    # POST /user/carts
    # POST /user/carts.json
    def create
      @cart = Cart.new(cart_params.merge(user: current_user))
      respond_to do |format|
        if @cart.save
          format.html { redirect_to user_carts_url, notice: 'Cart was successfully created.' }
          format.json { render :show, status: :created, location: user_carts_url }
        else
          format.html { render :new }
          format.json { render json: @cart.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /user/carts/1
    # PATCH/PUT /user/carts/1.json
    def update
      respond_to do |format|
        if @cart.update(cart_params)
          format.html { redirect_to [:user, @cart], notice: 'Cart was successfully updated.' }
          format.json { render :show, status: :ok, location: [:user, @cart] }
        else
          format.html { render :edit }
          format.json { render json: @cart.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /user/carts/1
    # DELETE /user/carts/1.json
    def destroy
      @cart.destroy
      respond_to do |format|
        format.html { redirect_to user_carts_url, notice: 'Cart was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_cart
        @cart = Cart.where(id: params[:id], user_id: current_user.id).first
        return invalid_access_flash_redirect_and_return if @cart.blank?
      end

      # Only allow a trusted parameter "white list" through.
      def cart_params
        default_values = {user_id: current_user.id}
        params.require(:cart).permit(:id_token, :user_process_id).merge(default_values)
      end
  end
end
