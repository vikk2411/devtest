module User
  class SubServicesController < ApplicationController
    before_action :set_sub_service, only: [:show, :edit, :update, :destroy]
    load_and_authorize_resource

    # GET /user/sub_services
    # GET /user/sub_services.json
    def index
      @sub_services = SubService.where(user_id: current_user.id)
    end

    # GET /user/sub_services/1
    # GET /user/sub_services/1.json
    def show
    end

    # GET /user/sub_services/new
    def new
      @sub_service = SubService.new
    end

    # GET /user/sub_services/1/edit
    def edit
    end

    # POST /user/sub_services
    # POST /user/sub_services.json
    def create
      @sub_service = SubService.new(sub_service_params)
      respond_to do |format|
        if @sub_service.save
          format.html { redirect_to user_sub_services_url, notice: 'Sub service was successfully created.' }
          format.json { render :show, status: :created, location: user_sub_services_url }
        else
          format.html { render :new }
          format.json { render json: @sub_service.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /user/sub_services/1
    # PATCH/PUT /user/sub_services/1.json
    def update
      respond_to do |format|
        if @sub_service.update(sub_service_params)
          format.html { redirect_to [:user, @sub_service], notice: 'Sub service was successfully updated.' }
          format.json { render :show, status: :ok, location: [:user, @sub_service] }
        else
          format.html { render :edit }
          format.json { render json: @sub_service.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /user/sub_services/1
    # DELETE /user/sub_services/1.json
    def destroy
      @sub_service.destroy
      respond_to do |format|
        format.html { redirect_to user_sub_services_url, notice: 'Sub service was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_sub_service
        @sub_service = SubService.where(id: params[:id], user_id: current_user.id).first
        return invalid_access_flash_redirect_and_return if @sub_service.blank?
      end

      # Only allow a trusted parameter "white list" through.
      def sub_service_params
        default_values = {user_id: current_user.id}
        params.require(:sub_service).permit(:name, :service_id).merge(default_values)
      end
  end
end
