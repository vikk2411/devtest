class CurrencyInput < SimpleForm::Inputs::Base
  def input(wrapper_options)
    template.content_tag(:div, class: 'input-group number') do
      template.concat span_currency
      template.concat @builder.text_field(attribute_name, input_html_options)
      
    end
  end

  def input_html_options
    super.merge({class: 'form-control'})
  end

  def span_currency
    template.content_tag(:span, class: 'input-group-addon') do
      template.concat icon_inr
    end
  end

  def icon_inr
    "<i class='fa fa-inr'></i>".html_safe
  end

end