class DateTimePickerInput < SimpleForm::Inputs::Base
  def input(wrapper_options)
    template.content_tag(:div, class: 'input-group date col-md-4') do
      template.concat @builder.text_field(attribute_name, input_html_options)
      template.concat span_table if input_html_options[:icon] == true
    end
  end

  def input_html_options
    super.merge({class: 'form-control form_datetime'})
  end

  def span_table
    template.content_tag(:span, class: 'input-group-addon') do
      template.concat icon_calendar
    end
  end

  def icon_calendar
    "<i class='fa fa-calendar'></i>".html_safe
  end

end