class User < ApplicationRecord
  
  after_initialize :set_default_role, :if => :new_record?

  enumex role: {
    customer:     [0, "Customer"], 
    professional: [1, "Professional"], 
    both:         [2, "Both"],
  }, _prefix: true

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :invitable, :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable

  def set_default_role
    self.role ||= :customer
  end

  def customer?
    return (self.role_customer? or self.role_both?)
  end

  def professional?
    return (self.role_professional? or self.role_both?)
  end
end
