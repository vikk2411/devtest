class ServiceFlow::UserAnswerChoice < ApplicationRecord
  belongs_to :user
  belongs_to :answer_choice
  belongs_to :user_process
end
