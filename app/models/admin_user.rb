class AdminUser < ApplicationRecord
  after_initialize :set_default_role, :if => :new_record?

  enumex role: {
    super_admin: [0, "Super Admin"],
    read_write:  [1, "Read Write"],
    read_only:   [2, "Read Only"],
  }, _prefix: true

  enumex status: {
    active:   [0, "Active"],
    inactive: [1, "Inactive"],
    deleted:  [2, "Deleted"],
  }, _prefix: true

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :invitable, :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable

  def set_default_role
    self.role ||= :read_only
  end
end

