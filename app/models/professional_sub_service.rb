class ProfessionalSubService < ApplicationRecord
  belongs_to :professional
  belongs_to :sub_service
  belongs_to :professional_service
end
