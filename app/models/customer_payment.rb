class CustomerPayment < ApplicationRecord
  belongs_to :user
  belongs_to :professional
  belongs_to :payable, polymorphic: true
  belongs_to :user_process
  belongs_to :scope
end
