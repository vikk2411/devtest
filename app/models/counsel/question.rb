class Counsel::Question < ApplicationRecord
  belongs_to :asker, polymorphic: true
end
