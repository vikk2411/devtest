class Counsel::Answer < ApplicationRecord
  belongs_to :question
  belongs_to :answerer, polymorphic: true
end
