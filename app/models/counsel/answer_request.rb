class Counsel::AnswerRequest < ApplicationRecord
  belongs_to :question
  belongs_to :requester, polymorphic: true
  belongs_to :answerer, polymorphic: true
end
