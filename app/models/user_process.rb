class UserProcess < ApplicationRecord
  belongs_to :user
  belongs_to :professional
  belongs_to :service
  belongs_to :sub_service
  belongs_to :scenario
end
