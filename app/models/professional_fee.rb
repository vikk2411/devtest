class ProfessionalFee < ApplicationRecord
  belongs_to :professional
  belongs_to :scenario
  belongs_to :location
end
