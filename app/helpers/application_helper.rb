module ApplicationHelper
  def fa(fontawesome_icon, options={})
    return content_tag(:i, class: "fa fa-#{fontawesome_icon} #{"fa-fw" if not options[:no_fw]} #{options[:extra_classes]}") {}
  end

  def new_page_heading(heading = "", &block)
    heading = capture(&block) if block
    html = <<-HTML
      <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
          <h2>#{heading}</h2>
        </div>
      </div>
    HTML
    return html.html_safe
  end
  
  def currency(amt)
    html = "#{number_to_currency(amt, unit: SCHOOL_CONFIG['currency_symbol'])}"
    return html.html_safe
  end

  def number_with_indian_comma(number)
    integer = number.to_s.gsub(",","").to_i
    fraction = number.to_s.split(".")[1]

    return number.to_s if integer < 1000

    retval = integer.to_s.reverse.gsub(/(^\d{3}|\d{2})(?=\d)/, '\\1,').reverse
    retval += ".#{fraction}" if not fraction.blank?

    return retval
  end

  def highlight_nav_link(data={})
    html = content_tag(:div, id: "forced-nav-link-highlight", data: data) {}
    return html.html_safe
  end

  def table(extra_classes="", id = "", &block)
    content = capture(&block)
    html = <<-HTML
      <table #{id.blank? ? '' : 'id='+id.to_s} class="table #{extra_classes}">
        #{content}
      </table>
    HTML
    return html.html_safe
  end

  def table_responsive(extra_classes="", id = "", &block)
    content = capture(&block)
    html = <<-HTML
      <div class="table-responsive">
        <table #{id.blank? ? '' : 'id='+id.to_s} class="table #{extra_classes}">
          #{content}
        </table>
      </div>
    HTML
    return html.html_safe
  end

  def full_width_box_wrapper(&block)
    content = capture(&block)
    html = <<-HTML
      <div class="row">
        <div class="col-lg-12">
          <div class="wrapper wrapper-content animated fadeInUp">
            #{content}
          </div>
        </div>
      </div>
    HTML
    return html.html_safe
  end

  def box_content_only(options = {}, &block)
    content = capture(&block)
    html = <<-HTML
      <div class="ibox-content m-b-sm border-bottom #{options[:extra_classes]}" #{%Q[style="#{options[:style]}"] if options[:style]}>
        #{content}
      </div>
    HTML
    return html.html_safe
  end

  def box(heading="", options={}, &block)
    icon = options[:icon].to_s
    name = options[:name].to_s

    default_options = {
      collapsed: false,
      collapsed_button_text: nil,
      uncollapsed_button_text: nil
    }
    options = default_options.merge(options)


    content = capture(&block)
    html = ""
    if not name.blank?
      html += <<-HTML
        <a name="#{name}"></a>
      HTML
    end

    chevron_up_html = <<-HTML
      <a class="collapse-link">
        <i class="fa fa-chevron-up"></i>
      </a>
    HTML

    collapsed_button_html = <<-HTML
      <a class="collapse-link btn btn-primary btn-outline">
        #{options[:collapsed_button_text]}
      </a>
    HTML
    
    html += <<-HTML
      <div class="ibox float-e-margins #{options[:collapsed] ? "collapsed" : ''}">
        <div class="ibox-title">
          <h5 #{options[:collapsed_button_text].present? ? "style='margin-top:7px'" : nil}>#{icon.blank? ? "" : fa(icon)}#{heading}</h5>
          <div class="ibox-tools">
            #{options[:collapsed_button_text].present? ? collapsed_button_html.html_safe : chevron_up_html.html_safe}
          </div>
        </div>
        <div class="ibox-content">
          #{content}
        </div>
      </div>
    HTML
    return html.html_safe
  end
  
  def new_row(options={}, &block)
    content = capture(&block)
    html = <<-HTML
      <div class="row #{options[:extra_classes]}" style="#{options[:style]}">
        #{content}
      </div>
    HTML
    return html.html_safe
  end

  def row(&block)
    content = capture(&block)
    html = <<-HTML
      <div class="row border-bottom white-bg dashboard-header">
        #{content}
      </div>
    HTML
    return html.html_safe
  end

  def col(size, extra_classes="", &block)
    content = capture(&block)
    html = <<-HTML
      <div class="col-md-#{size} #{extra_classes}">
        #{content}
      </div>
    HTML
    return html.html_safe
  end

  def solo_column(&block)
    content = capture(&block)
    html = <<-HTML
      <div class="col-lg-12">
        #{content}
        <br>
        <br>
      </div>

    HTML
    return html.html_safe
  end

  def center_column(&block)
    content = capture(&block)
    html = <<-HTML
      <div class="col-lg-8">
        #{content}
        <br>
        <br>
      </div>
    HTML
    return html.html_safe
  end

  def right_column(&block)
    content = capture(&block)
    html = <<-HTML
      <div class="col-lg-4">
        #{content}
        <br>
        <br>
      </div>
    HTML
    return html.html_safe
  end

  def page_heading(heading)
    html = <<-HTML
    <h2  class="page-header">#{heading}</h2>
    HTML
    return html.html_safe
  end

  def breadcrumb_bar(&block)
    content = capture(&block)
    html = <<-HTML
      <div class="row pad-top-10 white-bg" style="padding-left:10px">
        <div class="col-lg-12">
          <ol class="breadcrumb mar-bot-0">
            #{content}
          </ol>
        </div>
      </div>
    HTML
    return html.html_safe
  end

  def breadcrumb_bar_new(&block)
    content = capture(&block)
    html = <<-HTML
      <ol class="breadcrumb mar-bot-0">
        #{content}
      </ol>
    HTML
    return html.html_safe
  end

  def breadcrumb_item(bc_info)
    if bc_info[:link].blank?
      html = <<-HTML
        <li class="active">#{bc_info[:text]}</li>
      HTML
    else
      html = <<-HTML
        <li><a href="#{bc_info[:link]}">#{bc_info[:text]}</a></li>
      HTML
    end
    return html.html_safe
  end

  def section_heading(heading = "", options = {})
    html = <<-HTML
      <div class="row" style="margin-bottom:10px;#{"margin-top:25px" if not options[:first]}">
        <div class="col-md-12"> 
          #{heading.upcase}
        </div>
      </div>
    HTML
    return html.html_safe
  end

  def page_datatable(options = {}, &block)
    options = {id: 'dataTables-example'}.merge(options)
    content = capture(&block)
    html = <<-HTML

      <div class="dataTable_wrapper">
        <table class="table table-striped table-bordered table-hover #{options[:extra_classes].to_a.join(" ")}" style="#{options[:extra_styles]}" id="#{options[:id]}">
          #{content}
        </table>
      </div>

    HTML
    return html.html_safe
  end

  def payment_gateway_button(transaction_id, amount, email, mobile, name)
    if SCHOOL_CONFIG['payment_gateway'] == "payumoney"
      payumoney_gateway_button(transaction_id, amount, email, mobile, name)
    elsif SCHOOL_CONFIG['payment_gateway'] == "paydummy"
      paydummy_gateway_button(transaction_id, amount, email, mobile, name)
    end
  end

  def paydummy_gateway_button(transaction_id, amount, email, mobile, name)
    html = <<-HTML
      <form id="payment-form" method="post" accept-charset="UTF-8" action="#{PAYDUMMY_CONFIG['payment_url']}">
        <input id="txnid" type="hidden" value="#{transaction_id}" name="txnid">
        <input id="amount" type="hidden" value="#{amount}" name="amount">
        <input id="firstname" type="hidden" value="#{name}" name="firstname">
        <input id="email" type="hidden" value="#{email}" name="email">
        <input id="mobile" type="hidden" value="#{mobile}" name="mobile">
        <input id="surl" type="hidden" value="#{PAYDUMMY_CONFIG['success_url']}" name="surl">
        <input id="furl" type="hidden" value="#{PAYDUMMY_CONFIG['failure_url']}" name="furl">
        <input type="submit" value=" Pay with Dummy ">
      </form>
    HTML
    return html.html_safe
  end

  def payumoney_gateway_button(transaction_id, amount, email, mobile, name)
    product_info = {"paymentParts" => [ {
                                            "name" => "StudyMaterial",
                                            "description" => "abcd",
                                            "value" => amount.to_s,
                                            "isRequired" => "true",
                                            "settlementEvent" => "EmailConfirmation"
                                          }
                                        ],
                      "paymentIdentifiers" => [ {
                                                  "field" => "CompletionDate",
                                                  "value" => "#{Time.now.to_s(:indian)}"
                                                },
                                                {
                                                  "field" => "TxnId",
                                                  "value" => transaction_id
                                                }
                                              ]
                      }
      payment_form_for_payu PAYU_CONFIG['merchant_key'], PAYU_CONFIG['merchant_salt'],
                    :txnid => transaction_id,
                    :amount => amount,
                    :productinfo => product_info.to_json,
                    :firstname => name,
                    :email => email,
                    :phone => mobile,
                    :surl => PAYU_CONFIG['success_url'],
                    :furl => PAYU_CONFIG['failure_url'],
                    :service_provider => 'payu_paisa',
                    :html => { :id => 'payment-form' }
  end

  def minute_to_time(min, format = "hh:mm", ampm = true)
    hh = min/60
    mm = min%60
    time_suffix = ""
    if ampm
      time_suffix = hh < 12 ? "am" : "pm"
      hh = hh < 13 ? hh : hh-12
    end
    formatted_time = format.gsub("hh", hh.to_s.rjust(2, '0')).gsub("mm", mm.to_s.rjust(2, '0'))
    if ampm
      formatted_time = formatted_time + " " + time_suffix
    end
  end

  def status_dot(status, options = {})
    default_options = {
      tooltip: true,
      extra_style: ""
    }
    options = default_options.merge(options)

    green_statuses  = ["Active", "Confirmed", "Settled"]
    orange_statuses = ["Created", "Pending", "Pending Submission", "Submitted", "Received", "Sent", "Dispatched", "Collected"]
    red_statuses    = ["Cancelled", "Failed", "Charged back", "Declined"]

    if green_statuses.include?(status)
      color_class = "text-info"
      color_style = ""
    elsif orange_statuses.include?(status)
      color_class = "text-warning"
      color_style = ""
    elsif red_statuses.include?(status)
      color_class = "text-danger"
      color_style = ""
    else
      color_class = ""
      color_style = ""
    end

    html = <<-HTML
      <span class="#{color_class}" style="#{color_style} #{options[:extra_style]}" #{%Q[data-toggle="tooltip" data-placement="top" title="#{status}"] if options[:tooltip]}>
        #{fa('circle')}
      </span>
    HTML
        
    return html.html_safe
  end

  def button_to_modal(name, options = {})
    options[:modal_id] ||= "modal-id"
    options[:class] ||= "btn-primary"
    html = <<-HTML
      <button type="button" class="btn #{options[:class]}" style="#{options[:style]}" data-toggle="modal" data-target="##{options[:modal_id]}">#{name}</button>
    HTML
    return html.html_safe
  end

  def link_to_modal(name, options = {})
    options[:modal_id] ||= "modal-id"
    options[:class] ||= "btn-primary"
    html = <<-HTML
      <a href="#" id="#{options[:id]}" class="#{options[:class]}" style="#{options[:style]}" data-toggle="modal" data-target="##{options[:modal_id]}">#{name}</a>
    HTML
    return html.html_safe
  end
  
  def business_type_options
    Company.business_types.map do |k, v| 
      case k.to_sym
      when :"LLP"
        ["Limited Liability Partnership", k]
      else
        [k.humanize.capitalize, k]
      end 
    end
  end

  def user_settings_path
    if user_signed_in?
      return edit_merchant_registration_path
    elsif admin_user_signed_in?
      return admin_edit_registration_path
    end     
  end

  def get_business_types_mappings
    company_id_mapping = Company.business_types.inject({}) do |h, (type, v)|
      case type.to_s
      when "Proprietorship"
        h[type] = "Proprietor's PAN"
      when "Society", "Trust"
        h[type] = "Registration Number"
      when "LLP"
        h[type] = "LLP Number"
      else
        h[type] = "CIN Number"
      end
      h
    end

    officer_name_mapping = Company.business_types.inject({}) do |h, (type, v)|
      case type.to_s
      when "Proprietorship"
        h[type] =  'Sole proprietor'
      when "Society"
        h[type] =  'Member'
      when "Trust"
        h[type] = 'Trustee' 
      when "Partnership", "LLP"
        h[type] = 'Partner'
      else
        h[type] = 'Director'
      end
      h
    end
    {:company_id_mapping => company_id_mapping, :officer_name_mapping => officer_name_mapping}
  end

  def modal_box(options={}, &block)
    default_options = {
      heading: "Add :heading option",
      button_text: "Save",
      button_color_class: "btn-primary",
      modal_id: "modal-id",
      button_id: "modal-button-id",
      button_href: "#",
      size: :small,
      buttons: true,
      heading_id: 'modal_heading'
    }
    options = default_options.merge(options)
    size_mappings = {
      small: '475px',
      medium: '640px',
      large: '720px'
    }
    link_options = {
      class: "btn #{options[:button_color_class]}", 
      id: options[:button_id], 
      style: "width:100%"
    }
    link_options = link_options.merge({method: options[:button_method]}) if options[:button_method]
    content = capture(&block)
    html = <<-HTML
      <div class="modal inmodal" id="#{options[:modal_id]}" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" style="width:#{size_mappings[options[:size]]}">
          <div class="modal-content animated fadeInUp">
            <div class="modal-header" style="padding:15px">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
              <h3 class="pull-left no-margins" id='#{options[:heading_id]}'>#{options[:heading]}</h3>
            </div>
            <div class="modal-body">
              #{content}
            </div>
            <div class="modal-footer">
    HTML
    if options[:buttons]
      html += <<-HTML
                <div class="row">
                  <div class="col-md-3">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
                  </div>
                  <div class="col-md-9">
                    #{link_to options[:button_text], options[:button_href],  link_options  }
                  </div>
                </div>
      HTML
    end
    html += <<-HTML
            </div>
          </div>
        </div>
      </div>
    HTML
    return html.html_safe
  end

  def user_logout_path
    if user_signed_in?
      destroy_user_session_path
    else
      destroy_admin_user_session_path
    end
  end

  def help_tooltip(help_text)
    html = <<-HTML
      <span data-toggle="tooltip" data-placement="right" title="#{help_text}">#{fa('question-circle')}</span>
    HTML
    return html.html_safe
  end

  def pdf_boxed_input(value_to_be_printed, single_box_width, extra_styles="")
    width = value_to_be_printed.to_s.length * single_box_width + 1
    html = <<-HTML
      <input type="text" style="width:#{ width }px;#{extra_styles}" value="#{ value_to_be_printed }" class="boxed_input"/>
    HTML
    return html.html_safe
  end

end
