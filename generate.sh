rails generate scaffold location name:string
rails generate scaffold service name:string
rails generate scaffold sub_service name:string service:references
rails generate scaffold scenario

rails generate scaffold service_flow/question question:text question_type:integer parent:integer:index
rails generate scaffold service_flow/answer_choice question:references{foreign_key{to_table=\"service_flow_questions\"}} answer:text position:integer next_questions:text

rails generate scaffold professional id_token:string:uniq user:references experience_start_at:datetime source:integer company_name:string location:integer photo:string about:text education:text experience:text clients:text aggregate_rating:decimal status:integer professional_type:integer other_details:text consult_rate:decimal platform_fee:text
rails generate scaffold professional_profile_edit professional:references experience_start_at:datetime company_name:string location:integer about:text company_profile:text education:text experience:text clients:text consult_rate:decimal
rails generate scaffold professional_service professional:references service:references
rails generate scaffold professional_sub_service professional:references sub_service:references professional_service:references
rails generate scaffold professional_fee professional:references scenario:references location:references status:integer professional_fee:decimal government_fee:decimal oope:decimal service_tax:boolean tax:decimal  

rails generate scaffold user_process id_token:string:uniq user:references professional:references service:references sub_service:references scenario:references status:integer external_link:text started_at:datetime closed_at:datetime expected_duration:integer
rails generate scaffold service_flow/user_answer_choice user:references answer_choice:references{foreign_key{to_table=\"service_flow_answer_choices\"}}:index{name=\"index_user_answer_choice_on_answer_choice_id\"} user_process:references
rails generate scaffold scope name:string user_process:references professional:references inclusions:text exclusions:text documents:text deliverables:text status:integer started_at:datetime closed_at:datetime expected_duration:integer
rails generate scaffold cart id_token:string:uniq user:references user_process:references
rails generate scaffold quote user_process:references scope:references cart:references status:integer professional_fee:decimal government_fee:decimal oope:decimal service_tax:boolean tax:decimal validity_end_date:datetime
rails generate scaffold customer_payment id_token:string:uniq user:references professional:references amount:decimal paid_amount:decimal payable:references{polymorphic} status:integer user_process:references scope:references

#payment_transaction

rails generate scaffold payment_request professional:references user_process:references scope:references amount:decimal status:integer rejection_reason:text approver_id:integer
rails generate scaffold review user:references professional:references rating:decimal review:text user_process:references
rails generate scaffold global_setting settings:text
rails generate scaffold counsel/category name:string
rails generate scaffold counsel/question question:string description:text asker:references{polymorphic}
rails generate scaffold counsel/question_category question:references{foreign_key{to_table=\"counsel_questions\"}} category:references{foreign_key{to_table=\"counsel_categories\"}}
rails generate scaffold counsel/answer question:references{foreign_key{to_table=\"counsel_questions\"}} answer:text answerer:references{polymorphic} accepted:boolean deleted:boolean
rails generate scaffold counsel/answer_request question:references{foreign_key{to_table=\"counsel_questions\"}} requester:references{polymorphic} answerer:references{polymorphic}
rails generate scaffold counsel/vote voter:references{polymorphic} votable:references{polymorphic} vote_type:integer
