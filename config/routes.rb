Rails.application.routes.draw do
  
  # Root
  root to: 'visitors#index', as: :home

  # Devise
  devise_for :users
  devise_for :admin_users

  # User
  scope module: 'user', as: :user do
    namespace :counsel do
      resources :votes
    end # For User
    namespace :counsel do
      resources :answer_requests
    end # For User
    namespace :counsel do
      resources :answers
    end # For User
    namespace :counsel do
      resources :question_categories
    end # For User
    namespace :counsel do
      resources :questions
    end # For User
    namespace :counsel do
      resources :categories
    end # For User
    resources :global_settings # For User
    resources :reviews # For User
    resources :payment_requests # For User
    resources :customer_payments # For User
    resources :quotes # For User
    resources :carts # For User
    resources :scopes # For User
    namespace :service_flow do
      resources :user_answer_choices
    end # For User
    resources :user_processes # For User
    resources :professional_fees # For User
    resources :professional_sub_services # For User
    resources :professional_services # For User
    resources :professional_profile_edits # For User
    resources :professionals # For User
    namespace :service_flow do
      resources :answer_choices
    end # For User
    namespace :service_flow do
      resources :questions
    end # For User
    resources :scenarios # For User
    resources :sub_services # For User
    resources :services # For User
    resources :locations # For User
    get 'dashboard', to: 'dashboard#index', as: :dashboard
  end

  # Admin
  namespace :admin do
    namespace :counsel do
      resources :votes
    end # For Admin
    namespace :counsel do
      resources :answer_requests
    end # For Admin
    namespace :counsel do
      resources :answers
    end # For Admin
    namespace :counsel do
      resources :question_categories
    end # For Admin
    namespace :counsel do
      resources :questions
    end # For Admin
    namespace :counsel do
      resources :categories
    end # For Admin
    resources :global_settings # For Admin
    resources :reviews # For Admin
    resources :payment_requests # For Admin
    resources :customer_payments # For Admin
    resources :quotes # For Admin
    resources :carts # For Admin
    resources :scopes # For Admin
    namespace :service_flow do
      resources :user_answer_choices
    end # For Admin
    resources :user_processes # For Admin
    resources :professional_fees # For Admin
    resources :professional_sub_services # For Admin
    resources :professional_services # For Admin
    resources :professional_profile_edits # For Admin
    resources :professionals # For Admin
    namespace :service_flow do
      resources :answer_choices
    end # For Admin
    namespace :service_flow do
      resources :questions
    end # For Admin
    resources :scenarios # For Admin
    resources :sub_services # For Admin
    resources :services # For Admin
    resources :locations # For Admin
    get 'dashboard', to: 'dashboard#index', as: :dashboard
  end

end
